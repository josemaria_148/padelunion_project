package com.sjava.padelunion;

public class Comentario {

    private int idcomentarios;
    private String comentarios;
    private int puntuacion;
    private int idpistas;
    private int idusuarios;

    public Comentario(int idcomentarios, String comentarios, int puntuacion, int idpistas, int idusuarios) {
        
        this.idcomentarios = idcomentarios;
        this.comentarios = comentarios;
        this.puntuacion = puntuacion;
        this.idpistas = idpistas;
        this.idusuarios = idusuarios;
        
        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de CentroController
        // CentroController.nuevoCentro(this);
    }

    public Comentario(String comentarios, int puntuacion, int idpistas, int idusuarios) {
        this.comentarios = comentarios;
        this.idpistas = idpistas;
        this.idusuarios = idusuarios;
        this.puntuacion = puntuacion;
        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de ComentarioController
        // ComentarioController.nuevoCentro(this);
    }

    public int getIdcomentarios(){
        return this.idcomentarios;
    }

    protected void setIdcomentarios(int idcomentarios){
        this.idcomentarios=idcomentarios;
    }
    public String getComentarios() {
        return this.comentarios;
    }

    protected void setComentarios(String comentarios){
        this.comentarios = comentarios;
    }

    public int getPuntuacion(){
        return this.puntuacion;
    }

    protected void setPuntuacion(int puntuacion){
        this.puntuacion=puntuacion;
    }

    public int getIdpistas(){
        return this.idpistas;
    }

    protected void setIdpistas(int idpistas){
        this.idpistas=idpistas;
    }
    public int getIdusuarios(){
        return this.idusuarios;
    }

    protected void setIdusuarios(int idusuarios){
        this.idusuarios=idusuarios;
    }
}