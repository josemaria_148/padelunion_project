package com.sjava.padelunion;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.google.gson.Gson;
import com.mysql.jdbc.Connection;


public class CentroController {

    private static final String TABLE = "centros";
	private static final String KEY = "idcentros";

   //getAll JSOn devuelve getAll en formato JSON
    // OJO! ver dependencia maven con <groupId>com.google.code.gson</groupId>

    public static String getAllJSON(){
        List<Centro> listaCentros = CentroController.getAll();
        Gson gson = new Gson();
        return gson.toJson(listaCentros);
    }


    // getAll devuelve la lista completa
    // public static List<Centro> getAll(){
    //     return listaCentros;
    // }
    public static List<Centro> getAll() {
        
        List<Centro> listaCentros = new ArrayList<Centro>();

        String sql = String.format("select %s,nombre,ubicacion,telefono,url,email,localidad,foto from %s", KEY, TABLE); // (2)
		
        try (Connection conn = DBConn.getConn();
        Statement stmt = conn.createStatement()) { 
    
        ResultSet rs = stmt.executeQuery(sql);  
        while (rs.next()) {
            Centro u = new Centro(
                        (Integer) rs.getObject(1),
                        (String) rs.getObject(2),
                        (String) rs.getObject(3),
                        (String) rs.getObject(4),
                        (String) rs.getObject(5),
                        (String) rs.getObject(6),
                        (String) rs.getObject(7),
                        (String) rs.getObject(8));
            listaCentros.add(u);
            }

        } catch (Exception e) { 
        String s = e.getMessage();
        System.out.println(s);
        }
        return listaCentros; 
    
    }

    //getId devuelve un registro
    // public static Centro getId(int id){
    //     for (Centro c : listaCentros) {
    //         if (c.getId()==id){
    //             return c;
    //         }
    //     }
    //     return null;
    // }

    public static Centro getId(int id) {

        Centro u = null;
        String sql = String.format("select %s,nombre,ubicacion,telefono,url,email,localidad,foto from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Centro(
                    (Integer) rs.getObject(1),
                    (String) rs.getObject(2),
                    (String) rs.getObject(3),
                    (String) rs.getObject(4),
                    (String) rs.getObject(5),
                    (String) rs.getObject(6),
                    (String) rs.getObject(7),
                    (String) rs.getObject(8));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }
   
    //guardar: guarda un centro
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    // public static void guardar(Centro cen) {
    //     if (cen.getId() == 0){
    //         contador++;
    //         cen.setId(contador);
    //         listaCentros.add(cen);
    //     } else {
    //         for (Centro c : listaCentros) {
    //             if (c.getId()==cen.getId()) {
                   
    //                 c.setNombre(cen.getNombre());
    //                 c.setUbicacion(cen.getUbicacion());
    //                 c.setTelefono(cen.getTelefono());
    //                 c.setUrl(cen.getUrl());
    //                 c.setEmail(cen.getEmail());
                  
    //                 break;
    //             }
    //         }
    //     }
        
    // }

 //save guarda un Centro
    // si es nuevo (id==0) lo añade a la BDD
    // si ya existe, actualiza los cambios
    public static void guardar(Centro c) {
        String sql;
        if (c.getId()>0) {
            sql = String.format("UPDATE %s set nombre=?, ubicacion=?, telefono=?, url=?, email=?, localidad=?, foto=? where %s=%d", TABLE, 
            KEY, c.getId(), c.getNombre(), c.getUbicacion(), c.getTelefono(), c.getUrl(), c.getEmail(), c.getLocalidad(), c.getFoto());
        }else {
            sql = String.format("INSERT INTO %s (nombre, ubicacion, telefono, url, email, localidad, foto) VALUES (?,?,?,?,?,?,?)", TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setString(1, c.getNombre());
            pstmt.setString(2, c.getUbicacion());
            pstmt.setString(3, c.getTelefono());
            pstmt.setString(4, c.getUrl());
            pstmt.setString(5, c.getEmail());
            pstmt.setString(6, c.getLocalidad());
            pstmt.setString(7, c.getFoto());
            pstmt.executeUpdate();
        if (c.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    c.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    // size devuelve numero de centros
    public static int size() {
       return 0;
    }


    // removeId elimina centro por id
    // public static void removeId(int id){
    //     Centro borrar=null;
    //     for (Centro c : listaCentros) {
    //         if (c.getId()==id){
    //             borrar = c;
    //             break;
    //         }
    //     }
    //     if (borrar!=null) {
    //         listaCentros.remove(borrar);
    //     }
    // }
     // removeId elimina alumno por id
     public static void removeId(int id){
         
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    

}