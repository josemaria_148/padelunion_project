package com.sjava.padelunion;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.google.gson.Gson;
import com.mysql.jdbc.Connection;

public class PistaController {

    private static final String TABLE = "pistas";
    private static final String KEY = "idpistas";
    
    //getAll JSOn devuelve getAll en formato JSON
    // OJO! ver dependencia maven con <groupId>com.google.code.gson</groupId>

    public static String getAllJSON(){
        List<Pista> listaPistas = PistaController.getAll();
        Gson gson = new Gson();
        return gson.toJson(listaPistas);
    }

    public static String getAllJSONT(){
        List<Pista> listaPistas = PistaController.getAllT();
        Gson gson = new Gson();
        return gson.toJson(listaPistas);
    }



    // getAll devuelve la lista completa
    public static List<Pista> getAll(){

            List<Pista> listaPistas = new ArrayList<Pista>();

            String sql = String.format("select %s,nombre,tipo,indoor,idcentros from %s", KEY, TABLE); 
		
            try (Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement()) { 
        
            ResultSet rs = stmt.executeQuery(sql);  
            while (rs.next()) {
                Pista u = new Pista(
                            rs.getInt(1),
                            rs.getString(2),
                            rs.getInt(3),
                            rs.getInt(4),
                            rs.getInt(5)
                            );
                listaPistas.add(u);
                }
    
            } catch (Exception e) { 
            String s = e.getMessage();
            System.out.println(s);
            }
            return listaPistas; 

    }

    //getId devuelve un registro
    public static Pista getId(int id){
        Pista u = null;
        String sql = String.format("select %s,nombre,tipo,indoor,idcentros from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Pista(
                    rs.getInt(1),
                    rs.getString(2),
                    rs.getInt(3),
                    rs.getInt(4),
                    rs.getInt(5));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }

    //getPistaCentro devuelve las pistas de un centro
    // public static List<Pista> getPistasCentro(int idcentro){

    //         List<Pista> listaPistasCentro = new ArrayList<Pista>();
    //         for (Pista p : listaPistasCentro) {
    //             if(p.getIdcentro() == idcentro) {
    //                     listaPistasCentro.add(p);
    //             }
    
    //         }
     
    //     return listaPistasCentro;
    // } 

    //guardar: guarda una pista
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void guardar(Pista c) {
        
        String sql;
        if (c.getId()>0) {
            sql = String.format("UPDATE %s set nombre=?, tipo=?, indoor=?, idcentros=? where %s=%d", TABLE, 
            KEY, c.getId(), c.getNombre(), c.getTipo(), c.getIndoor(), c.getIdcentro());
        }else {
            sql = String.format("INSERT INTO %s (nombre,tipo,indoor,idcentros) VALUES (?,?,?,?)", TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
                    
                pstmt.setString(1, c.getNombre());                
                pstmt.setInt(2, c.getTipo());
                pstmt.setInt(3, c.getIndoor());
                pstmt.setInt(4, c.getIdcentro());

                pstmt.executeUpdate();
        if (c.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    c.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }

    // numeroPistas devuelve numero de centros
    public static int numeroPistas(int idCentro) {
      
        int cantidad = 0;
       // System.out.println("aqui llega");
        String sql = String.format("select count(*) from %s where idcentros=%d", TABLE, idCentro);
        //System.out.println("aqui ahora");
        try (Connection conn = DBConn.getConn();
            Statement stmt = conn.createStatement()) {

            ResultSet rs  = stmt.executeQuery(sql);
        
                
                  if(rs.next()) {
                 
                         cantidad = rs.getInt(1);
                         }
              
        
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
       

        return cantidad;
        
    }


    // removeId elimina centro por id
    public static void removeId(int id){
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }




    public static List<Pista> getAllT(){

        List<Pista> listaPistas = new ArrayList<Pista>();

        String sql = String.format("select p.%s,nombre,tipo,indoor,idcentros, count(c.idcomentarios) cnt from %s p left join comentarios_pista c on c.idpistas=p.idpistas group by p.idpistas", KEY, TABLE); 

        try (Connection conn = DBConn.getConn();
        Statement stmt = conn.createStatement()) { 

        ResultSet rs = stmt.executeQuery(sql);  
        while (rs.next()) {
            Pista u = new Pista(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getInt(3),
                        rs.getInt(4),
                        rs.getInt(5),
                        rs.getInt(6)
                        );
            listaPistas.add(u);
            }

        } catch (Exception e) { 
        String s = e.getMessage();
        System.out.println(s);
        }
        return listaPistas; 

    }
}