package com.sjava.padelunion;

import java.util.ArrayList;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;


public class ComentarioController {

    private static final String TABLE = "comentarios_pista";
	private static final String KEY = "idcomentarios";

   


    // getAll devuelve la lista completa
    // public static List<Centro> getAll(){
    //     return listaCentros;
    // }
    public static List<Comentario> getAll() {
        
        List<Comentario> listaComentarios = new ArrayList<Comentario>();

        String sql = String.format("select %s,comentarios,puntuacion,idpistas,idusuarios from %s", KEY, TABLE); 
		
        try (Connection conn = DBConn.getConn();
        Statement stmt = conn.createStatement()) { 
    
        ResultSet rs = stmt.executeQuery(sql);  
        while (rs.next()) {
            Comentario u = new Comentario(
                        (Integer) rs.getObject(1),
                        (String) rs.getObject(2),
                        (Integer) rs.getObject(3),
                        (Integer) rs.getObject(4),
                        (Integer) rs.getObject(5));
                       
            listaComentarios.add(u);
            }

        } catch (Exception e) { 
        String s = e.getMessage();
        System.out.println(s);
        }
        return listaComentarios; 
    
    }

    //getId devuelve un registro
    // public static Centro getId(int id){
    //     for (Centro c : listaCentros) {
    //         if (c.getId()==id){
    //             return c;
    //         }
    //     }
    //     return null;
    // }

    public static Comentario getId(int idcomentarios) {

        Comentario u = null;
        String sql = String.format("select %s,comentarios,puntuacion,idpistas,idusuarios from %s where %s=%d", KEY, TABLE, KEY, idcomentarios);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Comentario(
                    (Integer) rs.getObject(1),
                    (String) rs.getObject(2),
                    (Integer) rs.getObject(3),
                    (Integer) rs.getObject(4),
                    (Integer) rs.getObject(5));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }
   
    //guardar: guarda un centro
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    // public static void guardar(Centro cen) {
    //     if (cen.getId() == 0){
    //         contador++;
    //         cen.setId(contador);
    //         listaCentros.add(cen);
    //     } else {
    //         for (Centro c : listaCentros) {
    //             if (c.getId()==cen.getId()) {
                   
    //                 c.setNombre(cen.getNombre());
    //                 c.setUbicacion(cen.getUbicacion());
    //                 c.setTelefono(cen.getTelefono());
    //                 c.setUrl(cen.getUrl());
    //                 c.setEmail(cen.getEmail());
                  
    //                 break;
    //             }
    //         }
    //     }
        
    // }

 //save guarda un Centro
    // si es nuevo (id==0) lo añade a la BDD
    // si ya existe, actualiza los cambios
    public static void guardar(Comentario c) {
        String sql;
        if (c.getIdcomentarios()>0) {
            sql = String.format("UPDATE %s set comentarios=?, puntuacion=?, idpistas=?, idusuarios=? where %s=%d", TABLE, 
            KEY, c.getIdcomentarios(), c.getComentarios(), c.getPuntuacion(), c.getIdpistas(), c.getIdusuarios());
        }else {
            sql = String.format("INSERT INTO %s (comentarios, puntuacion, idpistas, idusuarios) VALUES (?,?,?,?)", TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setString(1, c.getComentarios());
            pstmt.setInt(2, c.getPuntuacion());
            pstmt.setInt(3, c.getIdpistas());
            pstmt.setInt(4, c.getIdusuarios());
            pstmt.executeUpdate();
        if (c.getIdcomentarios()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    c.setIdcomentarios(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
    
    // size devuelve numero de centros
    public static int size() {
       return 0;
    }


    // removeId elimina centro por id
    // public static void removeId(int id){
    //     Centro borrar=null;
    //     for (Centro c : listaCentros) {
    //         if (c.getId()==id){
    //             borrar = c;
    //             break;
    //         }
    //     }
    //     if (borrar!=null) {
    //         listaCentros.remove(borrar);
    //     }
    // }
     // removeId elimina alumno por id
     public static void removeId(int id){
         
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }




    public static List<Comentario> getComentariosPorPista(int id) {
        
    List<Comentario> listaComentarios = new ArrayList<Comentario>();

    String sql = String.format("select %s,comentarios,puntuacion,idpistas,idusuarios from %s where idpistas=%d", KEY, TABLE,id); 
    
    try (Connection conn = DBConn.getConn();
    Statement stmt = conn.createStatement()) { 

    ResultSet rs = stmt.executeQuery(sql);  
    while (rs.next()) {
        Comentario u = new Comentario(
                    (Integer) rs.getObject(1),
                    (String) rs.getObject(2),
                    (Integer) rs.getObject(3),
                    (Integer) rs.getObject(4),
                    (Integer) rs.getObject(5));
                   
        listaComentarios.add(u);
        }

    } catch (Exception e) { 
    String s = e.getMessage();
    System.out.println(s);
    }
    return listaComentarios; 
    }

    //Devuelve cantidad de comentarios
    public static int getCantidad(int idpista) {

        int cantidad = 0;
        // System.out.println("aqui llega");
         String sql = String.format("select count(*) from %s where idpistas=%d", TABLE, idpista);
         //System.out.println("aqui ahora");
         try (Connection conn = DBConn.getConn();
             Statement stmt = conn.createStatement()) {
 
             ResultSet rs  = stmt.executeQuery(sql);
         
                 
                   if(rs.next()) {
                  
                          cantidad = rs.getInt(1);
                          }
               
         
         } catch (Exception e) {
             String s = e.getMessage();
             System.out.println(s);
         }
        
 
         return cantidad;
         
    }
    public static String puntuacionEscrita(int puntu) {
       
        String puntuacion = "No";

        switch ( puntu ) {
            
            case 0:
                puntuacion = "Muy Mala";
                 break;
            case 1:
                 puntuacion= "Mala";
                 break;
            case 2:
                puntuacion= "Regular";
                 break;
            case 3:
                 puntuacion= "Buena";
                 break;
            case 4:
                 puntuacion= "Muy Buena";
                 break;
            case 5:
                 puntuacion= "Excelente";
                 break;
            default:
                 puntuacion="No";
                 break;
            }
            return puntuacion;
    } 

    public static int calcularMedia(int idpistas) {
        int media=0;
        int cantidad = 0;
        int sumapuntuacion = 0;
        if(getCantidad(idpistas)!=0){
        cantidad = getCantidad(idpistas);
         }
         List<Comentario> listaComentarios = ComentarioController.getComentariosPorPista(idpistas);

         for (Comentario c : listaComentarios) {

            sumapuntuacion = sumapuntuacion + c.getPuntuacion();
         }

        media = sumapuntuacion/cantidad;
        return media;

    }
}