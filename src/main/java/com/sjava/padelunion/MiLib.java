package com.sjava.padelunion;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MiLib{


    public static Date stringToDate(String sdata){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd"); 
        Date theDate = null;
            try {
                theDate = df.parse(sdata);
            } catch (ParseException e) {
                //e.printStackTrace();
            }
        return theDate;
    }

    public static String fechatoString(Date d){

       
       SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM");
       String fecha = sdf.format(d);
       return fecha;

    }

    public static int horario (String valor){

        int resultadoHora = 0;
        valor = valor.substring(0,2);
        int horaPartido = Integer.parseInt(valor);

        if(horaPartido>=8 && horaPartido<=14){
            resultadoHora = 0;
        } else if(horaPartido>14 && horaPartido<=21){
            resultadoHora = 1;
        } else{
            resultadoHora = 2;
        }

        return  resultadoHora;

    }


}
