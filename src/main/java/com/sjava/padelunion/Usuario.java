package com.sjava.padelunion;

import java.util.Date;

public class Usuario {

    private int id;
    private String nombre;
    private String apellidos;
    private String email;
    private Date fechanacimiento;
    private int nivel;
    private int mano;
    private int sexo;
    private int administrador;
    private String telefono;
    private int ranking;
    private String clave;

    public Usuario(String email, String clave) {
        this.email = email;
        this.clave = clave;

        // cambio! no se añade de forma automática, se obliga a ejecutar el método
        // "guardar" de UsuarioController

    }
    public Usuario(String email, int id){
        this.email = email;
        this.id = id;

    }

    public Usuario(int id, String email) {
        this.id = id;
        this.email = email;
       

    }
    public Usuario(String nombre, String apellidos, String email, String clave, Date fechanacimiento, int nivel, int mano, int sexo, 
             int administrador, String telefono) {

        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.clave = clave;
        this.fechanacimiento = fechanacimiento;
        this.nivel = nivel;
        this.mano = mano;
        this.sexo = sexo;
        this.administrador = administrador;
        this.telefono = telefono;

    }

    public Usuario(int id,String nombre, String apellidos, String email, String clave, Date fechanacimiento, int nivel, int mano, int sexo, 
    int administrador, String telefono) {

        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.clave = clave;
        this.fechanacimiento = fechanacimiento;
        this.nivel = nivel;
        this.mano = mano;
        this.sexo = sexo;
        this.administrador = administrador;
        this.telefono = telefono;

    }

    public Usuario(int id,String nombre, String apellidos, String email, Date fechanacimiento, int nivel, int mano, int sexo, 
    int administrador, String telefono) {

        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.email = email;
        this.fechanacimiento = fechanacimiento;
        this.nivel = nivel;
        this.mano = mano;
        this.sexo = sexo;
        this.administrador = administrador;
        this.telefono = telefono;

    }

    // añadimos getter
    public String getClave(){
        return this.clave;
    }

    public int getId() {
        return this.id;
    }

    protected void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre) {
        this.nombre = nombre;
    }

    // public String getPassword() {
    //     return this.password;
    // }

    // protected void setPassword(String password) {
    //     this.password = password;
    // }

    public String getApellidos() {
        return this.apellidos;
    }

    protected void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getEmail() {
        return this.email;
    }

    protected void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return this.telefono;
    }

    protected void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public int getNivel() {
        return this.nivel;
    }

    protected void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public int getMano() {
        return this.mano;
    }

    protected void setMano(int mano) {
        this.mano = mano;
    }

    public int getAdministrador() {
        return this.administrador;
    }

    protected void setAdministrador(int administrador) {
        this.administrador = administrador;
    }

    public int getRanking() {
        return this.ranking;
    }

    protected void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public int getSexo() {
        return this.sexo;
    }

    protected void setSexo(int sexo) {
        this.sexo = sexo;
    }

    public Date getFechaNacimiento() {
        return this.fechanacimiento;
    }

    protected void setFechaNacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

}