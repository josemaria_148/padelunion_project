package com.sjava.padelunion;

public class Pista {

    private int idpistas;
    private String nombre;
    private int tipo;
    private int indoor;
    private int idcentros;
    private int numcomentarios;

    public Pista(int idpistas, String nombre, int tipo, int indoor, int idcentros) {
        this.idpistas = idpistas;
        this.nombre = nombre;
        this.tipo = tipo;
        this.indoor = indoor;
        this.idcentros = idcentros;

        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guardar" de PistaController
        // PistaController.nuevoPista(this);
    }

    
    public Pista(int idpistas, String nombre, int tipo, int indoor, int idcentros, int numcomentarios) {
        this.idpistas = idpistas;
        this.nombre = nombre;
        this.tipo = tipo;
        this.indoor = indoor;
        this.idcentros = idcentros;
        this.numcomentarios = numcomentarios;

        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guardar" de PistaController
        // PistaController.nuevoPista(this);
    }


    public Pista(String nombre, int tipo, int indoor, int idcentros) {
        
        this.nombre = nombre;
        this.tipo = tipo;
        this.indoor = indoor;
        this.idcentros = idcentros;

        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guardar" de PistaController
        // PistaController.nuevoPista(this);
    }


    public int getId(){
        return this.idpistas;
    }

    protected void setId(int idpistas){
        this.idpistas=idpistas;
    }

    public String getNombre() {
        return this.nombre;
    }

    protected void setNombre(String nombre){
        this.nombre = nombre;
    }

    public int getTipo(){
        return this.tipo;
    }
    
    protected void setTipo(int tipo){
        this.tipo = tipo;
    }

    public int getIndoor(){
        return this.indoor;
    }

    protected void setIndoor(int indoor){
        this.indoor = indoor;
    }

    public int getIdcentro(){
        return this.idcentros;
    }

    protected void setIdcentro(int idcentros){
        this.idcentros = idcentros;
    }
  
}