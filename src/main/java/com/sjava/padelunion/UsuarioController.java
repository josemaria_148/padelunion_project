package com.sjava.padelunion;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.mysql.jdbc.Connection;

public class UsuarioController {

    private static final String TABLE = "usuarios";
	private static final String KEY = "idusuarios";


    // getAll devuelve la lista completa
    public static List<Usuario> getAll(){

        List<Usuario> listaUsuarios = new ArrayList<Usuario>();

        String sql = String.format("select %s,nombre,apellidos,email,clave,fechanacimiento,nivel,mano,sexo,administrador,telefono from %s", KEY, TABLE); // (2)
		
        try (Connection conn = DBConn.getConn();
        Statement stmt = conn.createStatement()) { 
    
        ResultSet rs = stmt.executeQuery(sql);  
        while (rs.next()) {

            Usuario u = new Usuario(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDate(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getString(11)
                        );
            listaUsuarios.add(u);
            }

        } catch (Exception e) { 
        String s = e.getMessage();

        System.out.println(s);
        }
        return listaUsuarios; 

    }

    //getId devuelve un registro Usuario
    public static Usuario getId(int id){
        Usuario u = null;
        String sql = String.format("select %s,nombre,apellidos,email,clave,fechanacimiento,nivel,mano,sexo,administrador,telefono from %s where %s=%d", KEY, TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Usuario(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getDate(6),
                        rs.getInt(7),
                        rs.getInt(8),
                        rs.getInt(9),
                        rs.getInt(10),
                        rs.getString(11)
                        );
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }

    //getPistaCentro devuelve las pistas de un centro

    //guardar: guarda una pista
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void guardar(Usuario c) {
        
        String sql;
        if (c.getId()>0) {
            sql = String.format("UPDATE %s set nombre=?, apellidos=?, email=?, fechanacimiento=?, nivel=?, mano=?, sexo=?, administrador=?, telefono=? where %s=%d", TABLE, 
            KEY, c.getId(), c.getNombre(), c.getApellidos(), c.getEmail(), c.getFechaNacimiento(), c.getNivel(), c.getMano(), c.getSexo(), c.getAdministrador(), c.getTelefono());
        } else {
            sql = String.format("INSERT INTO %s (nombre, apellidos, email, fechanacimiento, nivel, mano, sexo, administrador, telefono) VALUES (?,?,?,?,?,?,?,?,?)", TABLE);
        }

        String sql2 = String.format("update %s set clave=password(?)  where %s=?", TABLE, KEY); //1

        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                PreparedStatement pstmt2 = conn.prepareStatement(sql2); //2
                Statement stmt = conn.createStatement()) {
            pstmt.setString(1, c.getNombre());
            pstmt.setString(2, c.getApellidos());
            pstmt.setString(3, c.getEmail());
           // pstmt.setString(4, c.getClave());

            long ms = c.getFechaNacimiento().getTime();
            java.sql.Date fechanacimiento_sql = new java.sql.Date(ms);

            pstmt.setDate(4, fechanacimiento_sql);
            pstmt.setInt(5, c.getNivel());
            pstmt.setInt(6, c.getMano());
            pstmt.setInt(7, c.getSexo());
            pstmt.setInt(8, c.getAdministrador());
            pstmt.setString(9, c.getTelefono());
            pstmt.executeUpdate();
        if (c.getId()==0) {
            //usuario nuevo, actualizamos el ID con el recién insertado
            ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    c.setId(rs.getInt(1));
                    pstmt2.setString(1, c.getClave()); //3
                    pstmt2.setInt(2, c.getId());
                    pstmt2.executeUpdate();
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }

    // size devuelve numero de centros
    // public static int size() {
    //     return listaUsuarios.size();
    // }


    // removeId elimina centro por id
    public static void removeId(int id){
        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn();
                Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static int checkLogin(String nombre, String clave){
        int resId = -1;    
        String sql = String.format("select %s from %s where email=? and clave=password(?)", KEY, TABLE); //1
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)) { 
            pstmt.setString(1, nombre);
            pstmt.setString(2, clave);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                    resId = rs.getInt(1); //2
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return resId;
    }

    //EresAdmin comprueba si un usuario es administrador
    public static int EresAdmin(int id){
        
        int respuesta = 0;

        String sql = String.format("select %s from %s where idusuarios=? and administrador=?", KEY, TABLE); 
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql)) { 
            pstmt.setInt(1, id);
            pstmt.setInt(2,1);
            ResultSet rs = pstmt.executeQuery();
            if (rs.next()) {
                    respuesta = rs.getInt(1); 
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        
        if(respuesta!=0){
              return 1;
             }else{return 0;}
    }
    

}