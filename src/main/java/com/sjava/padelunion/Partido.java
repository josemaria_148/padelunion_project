package com.sjava.padelunion;

import java.util.Date;

public class Partido {

    private int idpartidos;
    private String hora;
    private Date dia;
    private String sitio;
    private int estado;
    private String resultado;
    private int idcreador;
    private int jugador1;
    private int jugador2;
    private int jugador3;
    private int jugador4;
    private int nivel;

    //Constructores

    public Partido(){}

    public Partido(int idpartidos, String hora, Date dia, String sitio, int estado, String resultado, int idcreador,
        int jugador1, int jugador2, int jugador3, int jugador4, int nivel) {
        this.idpartidos = idpartidos;
        this.hora = hora;
        this.dia = dia;
        this.sitio = sitio;
        this.estado = estado;
        this.resultado = resultado;
        this.idcreador = idcreador;
        this.jugador1 = jugador1;
        this.jugador2 = jugador2;
        this.jugador3 = jugador3;
        this.jugador4 = jugador4;
        this.nivel = nivel;
        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de PartidoController
    }

    public Partido(String hora, Date dia, String sitio, int estado, String resultado, int idcreador,
        int jugador1, int jugador2, int jugador3, int jugador4, int nivel) {

        this.hora = hora;
        this.dia = dia;
        this.sitio = sitio;
        this.estado = estado;
        this.resultado = resultado;
        this.idcreador = idcreador;
        this.jugador1 = jugador1;
        this.jugador2 = jugador2;
        this.jugador3 = jugador3;
        this.jugador4 = jugador4;
        this.nivel = nivel;
        // cambio! no se añade de forma automática, se obliga a ejecutar el método "guarda" de PartidoController
    }

    public int getId(){
        return this.idpartidos;
    }

    protected void setId(int idpartidos){
        this.idpartidos=idpartidos;
    }

    public int getIdCreador() {
        return this.idcreador;
    }

    protected void setIdCreador(int idcreador){
        this.idcreador = idcreador;
    }

    public String getHora(){
        return this.hora;
    }

    protected void setHora(String hora){
        this.hora = hora;
    }

    public Date getDia(){
        return this.dia;
    }

    protected void setDia(Date dia){
        this.dia = dia;
    }

    public String getSitio(){
        return this.sitio;
    }
    
    protected void setSitio(String sitio){
        this.sitio = sitio;
    }

    public int getEstado(){
        return this.estado;
    }

    public void setEstado(int estado){
        this.estado=estado;
    }
 
    public int getJugador1(){
        return this.jugador1;
    }

    public void setJugador1(int jugador1){
        this.jugador1=jugador1;
    }
    public int getJugador2(){
        return this.jugador2;
    }

    public void setJugador2(int jugador2){
        this.jugador2=jugador2;
    }
    public int getJugador3(){
        return this.jugador3;
    }

    public void setJugador3(int jugador3){
        this.jugador3=jugador3;
    }
    public int getJugador4(){
        return this.jugador4;
    }

    public void setJugador4(int jugador4){
        this.jugador4=jugador4;
    }

    public int getNivel(){
        return this.nivel;
    }

    public void setNivel(int nivel){
        this.nivel=nivel;
    }

    public String getResultado(){
        return this.resultado;
    }

    protected void setResultado(String resultado){
        this.resultado=resultado;
    }
    // @Override
    // public String toString() {
    //     return String.format("%d ", this.id);
    // }
  
}