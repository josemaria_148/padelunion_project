package com.sjava.padelunion;

import java.util.ArrayList;
import java.util.List;
//import java.util.Date;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import com.google.gson.*;
import com.mysql.jdbc.Connection;

public class PartidoController {

    private static final String TABLE = "partidos";
    private static final String KEY = "idpartidos";

    // getAll JSOn devuelve getAll en formato JSON
    // OJO! ver dependencia maven con <groupId>com.google.code.gson</groupId>

    public static String getAllJSON() {
        List<Partido> listaPartidos = PartidoController.getAll();
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        return gson.toJson(listaPartidos);
    }

    public static String getMisPartidosJSON(int id) {
        List<Partido> listaPartidos = PartidoController.getMisPartidos(id);
        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
        return gson.toJson(listaPartidos);
    }

    // getAll devuelve la lista completa
    public static List<Partido> getAll() {

        List<Partido> listaPartidos = new ArrayList<Partido>();

        String sql = String.format(
                "select %s,hora,dia,sitio,estado,resultado,idcreador,jugador1,jugador2,jugador3,jugador4,nivel from %s",
                KEY, TABLE);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Partido u = new Partido(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getString(4), rs.getInt(5),
                        rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getInt(11),
                        rs.getInt(12));
                listaPartidos.add(u);
            }

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaPartidos;

    }

    // getId devuelve un registro
    public static Partido getId(int idpartidos) {
        Partido u = null;
        String sql = String.format(
                "select %s,hora,dia,sitio,estado,resultado,idcreador,jugador1,jugador2,jugador3,jugador4,nivel from %s where %s=%d",
                KEY, TABLE, KEY, idpartidos);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.next()) {
                u = new Partido(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getString(4), rs.getInt(5),
                        rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getInt(11),
                        rs.getInt(12));
            }
        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return u;
    }

    // getMispartidos devuelve la lista de partidos donde esta el usuario
    // public static List<Partido> getMispartidos(int idusuario){

    // List<Partido> listaMispartidos = new ArrayList<Partido>();
    // for (Partido p : listaPartidos) {
    // if(p.getCreador()==idusuario || p.getJugador1()==idusuario ||
    // p.getJugador2()==idusuario || p.getJugador3()==idusuario ||
    // p.getJugador4()==idusuario){
    // listaMispartidos.add(p);
    // }

    // }
    // return listaMispartidos;
    // }

    // guarda: guarda un partido
    // si es nuevo (id==0) lo añade a la lista
    // si ya existe, actualiza los cambios
    public static void guarda(Partido c) {

        String sql;
        if (c.getId() > 0) {
            sql = String.format(
                    "UPDATE %s set hora=?, dia=?, sitio=?, estado=?, resultado=?, idcreador=?, jugador1=?,jugador2=?,jugador3=?,jugador4=?,nivel=?  where %s=%d",
                    TABLE, KEY, c.getId(), c.getHora(), c.getDia(), c.getSitio(), c.getEstado(), c.getResultado(),
                    c.getIdCreador(), c.getJugador1(), c.getJugador2(), c.getJugador3(), c.getJugador4(), c.getNivel());
        } else {
            sql = String.format(
                    "INSERT INTO %s (hora, dia, sitio, estado, resultado, idcreador, jugador1, jugador2, jugador3, jugador4, nivel) VALUES (?,?,?,?,?,?,?,?,?,?,?)",
                    TABLE);
        }
        try (Connection conn = DBConn.getConn();
                PreparedStatement pstmt = conn.prepareStatement(sql);
                Statement stmt = conn.createStatement()) {
            pstmt.setString(1, c.getHora());

            long ms = c.getDia().getTime();
            java.sql.Date dia_sql = new java.sql.Date(ms);
            pstmt.setDate(2, dia_sql);

            pstmt.setString(3, c.getSitio());
            pstmt.setInt(4, c.getEstado());
            pstmt.setString(5, c.getResultado());
            pstmt.setInt(6, c.getIdCreador());
            pstmt.setInt(7, c.getJugador1());
            pstmt.setInt(8, c.getJugador2());
            pstmt.setInt(9, c.getJugador3());
            pstmt.setInt(10, c.getJugador4());
            pstmt.setInt(11, c.getNivel());

            pstmt.executeUpdate();
            if (c.getId() == 0) {
                // usuario nuevo, actualizamos el ID con el recién insertado
                ResultSet rs = stmt.executeQuery("select last_insert_id()");
                if (rs.next()) {
                    c.setId(rs.getInt(1));
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    // // size devuelve numero de partidos
    // public static int size() {
    // return listaPartidos.size();
    // }

    // removeId elimina partido por id
    public static void removeId(int id) {

        String sql = String.format("DELETE FROM %s where %s=%d", TABLE, KEY, id);
        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {
            stmt.executeUpdate(sql);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    // getAll devuelve la lista completa
    public static List<Partido> getFiltrar(String hora, String dia, String sitio, int estado) {

        List<Partido> listaPartidos = new ArrayList<Partido>();

        String sql = String.format(
                "select %s,hora,dia,sitio,estado,resultado,idcreador,jugador1,jugador2,jugador3,jugador4,nivel from %s where estado=%d",
                KEY, TABLE, estado);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Partido u = new Partido(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getString(4), rs.getInt(5),
                        rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getInt(11),
                        rs.getInt(12));
                listaPartidos.add(u);
            }

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaPartidos;

    }

    // getMisPartidos devuelve la lista completa
    public static List<Partido> getMisPartidos(int id) {

        List<Partido> listaPartidos = new ArrayList<Partido>();

        String sql = String.format(
                "select %s,hora,dia,sitio,estado,resultado,idcreador,jugador1,jugador2,jugador3,jugador4,nivel from %s where jugador1=%d or jugador2=%d or jugador3=%d or jugador4=%d or idcreador=%d",
                KEY, TABLE, id, id, id, id, id);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                Partido u = new Partido(rs.getInt(1), rs.getString(2), rs.getDate(3), rs.getString(4), rs.getInt(5),
                        rs.getString(6), rs.getInt(7), rs.getInt(8), rs.getInt(9), rs.getInt(10), rs.getInt(11),
                        rs.getInt(12));
                listaPartidos.add(u);
            }

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return listaPartidos;

    }

    // getDameIdcreador devuelve el nombre del creador del partido
    public static String getDameEmailCreador(int idpartido) {

        String email = "";
        int idcreador = 0;
        String sql = String.format("select idcreador from %s where id=%d", TABLE, idpartido);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {

                idcreador = rs.getInt(1);

            }

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }

        String sql2 = String.format("select email from usuarios where id=%d", idcreador);

        try (Connection conn = DBConn.getConn(); Statement stmt = conn.createStatement()) {

            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {

                email = rs.getString(1);

            }

        } catch (Exception e) {
            String s = e.getMessage();
            System.out.println(s);
        }
        return email;

    }

}
