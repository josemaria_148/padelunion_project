package com.sjava.padelunion;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class DBConn {
	
	private static final String URL = "jdbc:mysql://localhost/padel";
	private static final String USERNAME = "padel";
	private static final String PASSWORD = "sjava18";
	// private static final String USERNAME = "root";
	// private static final String PASSWORD = "admin";
	

	public static Connection getConn() throws SQLException {
		DriverManager.registerDriver(new com.mysql.jdbc.Driver ());
		return (Connection) DriverManager.getConnection(URL, USERNAME, PASSWORD);
	}
	
}
