package com.sjava.padelunion;


public class CalculaNivel {
   
    //Calcula el porcentaje en nivel
    public static int calcularNivel(int[] a) {
       
        int nivel=0;
        if(a[1]==1){
            nivel=+5;
        }
        nivel = nivel + calculaFisico(a[0],a[5],a[6]);
        nivel = nivel + calculaEntreno(a[4],a[6],a[7]);
        nivel = nivel + calculaExperiencia(a[2], a[3]);
        nivel = nivel + calculaTecnica(a[8],a[9],a[10],a[11],a[12],a[13],a[14]);
        return nivel;
    }
    //El 15% del nivel depende del fisico
    public static int calculaFisico(int edad, int fisico, int partidos ) {
        int nivelfisico=0;
        if(edad==3 || edad ==4 || edad==5){
            nivelfisico=nivelfisico+5;
        }
        nivelfisico= fisico + partidos;
        return nivelfisico;
    }
    //El 24% del nivel depende del entrenamiento
    public static int calculaEntreno(int entreno, int competicion, int partidos){
        int nivelentreno=0;
        nivelentreno = (partidos * competicion) + entreno; 
        return nivelentreno;

    }
    //El 20% del nivel depende de la experiencia
    public static int calculaExperiencia(int experiencia, int experienciatenis) {
        int nivelexperiencia=0;
        nivelexperiencia = experiencia*experienciatenis;
        return nivelexperiencia;

    }
    //El 35% del nivel depende de la tecnica
    public static int calculaTecnica(int drive, int reves, int saque, int resto, int volea, int rebotes, int globos) {
        int niveltecnico=0;
        niveltecnico = drive+reves+saque+resto+volea+rebotes+globos;
        return niveltecnico;

    }
    public static int nivelNumero(int nivel){
        int numNivel=0;
        if(nivel<20){
            numNivel=1;
        } else if(nivel>=20 && nivel<35){
            numNivel=2;
        } else if(nivel>=35 && nivel<50){
            numNivel=3;
        } else if(nivel>=50 && nivel<65){
            numNivel=4;
        } else if(nivel>=65 && nivel<80){
            numNivel=5;
        } else if(nivel>=80 && nivel<90){
            numNivel=6;
        } else if(nivel>=90){
            numNivel=7;
        } 
        return numNivel;
    }
    public static String nivel(int nivel) {
       
        String nivel_letra;

        switch ( nivel ) {
            
            case 1:
                 nivel_letra= "Iniciación (equivale al valor D)";
                 break;
            case 2:
                 nivel_letra= "Intermedio (equivale al valor C)";
                 break;
            case 3:
                nivel_letra= "Intermedio alto (equivale al valor C+)";
                 break;
            case 4:
                 nivel_letra= "Avanzado (equivale al valor B)";
                 break;
            case 5:
                 nivel_letra= "Competición (equivale al valor B+)";
                 break;
            case 6:
                 nivel_letra= "Profesional (equivale al valor A)";
                 break;
            case 7:
                 nivel_letra= "Profesional máximo (equivale al valor (A))";
                 break;
            default:
                 nivel_letra="X";
                 break;
            }
            return nivel_letra;
    }
    

}
