<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.sjava.padelunion.*" %>

<%

    Comentario c = null;
            
    //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
    request.setCharacterEncoding("UTF-8");

    String id = request.getParameter("id");
    if (id==null){
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/padelunion");
    }else{
        // recibimos id, puede ser que
        // 1) llegue por GET, desde list.jsp: mostraremos los datos para que puedan ser editados
        // 2) llegue por POST, junto con el resto de datos, para guardar los cambios

        //verificamos si la petición procede de un POST
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            // hemos recibido un POST, reccpilamos el resto de datos...
                
                int id_numerico = Integer.parseInt(id);
                String comentarios = request.getParameter("comentarios");
                int puntuacion = Integer.parseInt(request.getParameter("puntuacion"));
                int idpistas = Integer.parseInt(request.getParameter("idpistas"));
                int idusuarios = Integer.parseInt(request.getParameter("idusuarios"));

                //creamos nuevo objeto alumno, que reemplazará al actual del mismo id
                c = new Comentario(id_numerico, comentarios, puntuacion, idpistas, idusuarios);
                ComentarioController.guardar(c);
                //redirigimos navegador a la página list.jsp
                response.sendRedirect("/padelunion/pista/listpistas.jsp");
        } else {
            // hemos recibido un GET, solo tenemos un id
            // pedimos los datos del alumno para mostrarlos en el formulario de edifión
            c = ComentarioController.getId(Integer.parseInt(id));
            if (c==null){
                //no recibimos id, debe ser un error... volvemos a index
             response.sendRedirect("/padelunion");
             }
        }
		
    }

%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="container">

<div class="row">
<div class="col">
<h1>Editar comentario</h1>
</div>
</div>


<div class="row">
<div class="col-md-8">


<form action="#" method="POST">
  <div class="form-group">
    <label for="comentarioInput">Comentario</label>
    <input  name="comentarios"  type="text" class="form-control" id="comentarioInput" value="<%= c.getComentarios() %> ">
  </div>
  <div class="form-group">
    <h6>Puntuación: <span id="demo"></span></h6>
    <input value="<%= c.getPuntuacion()%>" name="puntuacion"  type="range" class="custom-range" min="0" max="5" id="myRange">

  </div>
  
 
  <!-- guardamos id en campo oculto! -->
    <input type="hidden" name="id" value="<%= c.getIdcomentarios() %>">
    <input  name="idpistas"  type="hidden" class="form-control" id="idpistasInput" value="<%= c.getIdpistas() %>">
    <input  name="idusuarios"  type="hidden" class="form-control" id="idusuariosInput" value="<%= c.getIdusuarios() %>">
     
      <div class="form-group">
    <button type="submit" class="btn btn-primary">Comentar</button>
    </div>
</form>


</div>
</div>

</div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>    

    <script>
        var slider = document.getElementById("myRange");
        var output = document.getElementById("demo");
        output.innerHTML = slider.value;

        slider.oninput = function() {
        output.innerHTML = this.value;
        }
    </script>

</body>
</html>
