<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>

 <%

 String idpistas = request.getParameter("idpista");
 int idpista=0;

    if (idpistas == null){
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/padelunion");
    }else{

       idpista = Integer.parseInt(idpistas);
    }

//inicializamos las variables loginId y loginName, por el momento sin valor
  int loginIdcom=0;
  String loginNamecom=null;


  if (session.getAttribute("loginId") != null) {

          loginIdcom = (Integer) session.getAttribute("loginId");
          if (loginIdcom>0){
            loginNamecom = (String) session.getAttribute("loginName");
          }

      }
int adminCom = UsuarioController.EresAdmin(loginIdcom);

%>
<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="container">

    <div class="row align-items-center">

      <div class="col-md-4"></div>            

        <div class="col-md-4 text-center">  

          <legend class="headerUserComentarios">Listado de 
          <%= ComentarioController.getCantidad(Integer.parseInt(idpistas))%>
           comentarios. Valoración media:
          <%= ComentarioController.puntuacionEscrita(ComentarioController.calcularMedia(Integer.parseInt(idpistas)))%> </legend>


        </div>

      <div class="col-md-4"></div>            
      
    </div>

<table class="table table-striped table-info table-bordered text-center" id="tabla_listado">
  <thead>
    <tr>
     
      <th scope="col">Nombre de la pista</th>
      <th scope="col">Comentario</th>
      <th scope="col">Puntuación</th>     
      <th scope="col">Usuario</th>
     
        <th scope="col"></th> 
     
          
    </tr>
  </thead>
  <tbody>
   <% for (Comentario c : ComentarioController.getComentariosPorPista(idpista)) { %>
       <% 
          Pista pista = PistaController.getId(c.getIdpistas());
          String nombrePista= pista.getNombre();
          Usuario usuario = UsuarioController.getId(c.getIdusuarios());
          String nombreUsuario= usuario.getEmail();

       %>
        <tr>
           
        <td><%= nombrePista %></td>
        <td><%= c.getComentarios() %></td>
        <td><%= ComentarioController.puntuacionEscrita(c.getPuntuacion()) %> </td>
        <td><%= nombreUsuario %></td>


     <% if (loginIdcom==c.getIdusuarios()) { %>
        <td><a href="/padelunion/comentario/edit.jsp?id=<%= c.getIdcomentarios() %>"><i class="fas fa-pencil-alt"></i>
</a></td>
     <%}%>
      <% if (adminCom==1) { %>
        <td><a href="/padelunion/comentario/remove.jsp?id=<%= c.getIdcomentarios() %>"><i class="fas fa-trash-alt"></i>
</a></td>
     <%}%>
        </tr>
    <%}%>
  </tbody>
</table>


</div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script> 

</body>
</html>
