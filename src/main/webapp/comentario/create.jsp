<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>
<%@page import="java.util.*" %>

<%
    boolean datosOk;

  Pista p = null;
            
    //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
    request.setCharacterEncoding("UTF-8");

    String id = request.getParameter("id");
    if (id==null){
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/padelunion");
    }else{
        // recibimos id, puede ser que
        // 1) llegue por GET, desde list.jsp: mostraremos los datos para que puedan ser editados
        // 2) llegue por POST, junto con el resto de datos, para guardar los cambios

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
     p = PistaController.getId(Integer.parseInt(id));
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo alumno
    
        //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
        request.setCharacterEncoding("UTF-8");

        String comentarios = request.getParameter("comentarios");
        int puntuacion = Integer.parseInt(request.getParameter("puntuacion"));
        int idpistas = Integer.parseInt(request.getParameter("idpistas"));
        int idusuarios = (Integer) session.getAttribute("loginId");

        
        // aquí verificaríamos que todo ok, y solo si todo ok hacemos guardar, 
        // por el momento lo damos por bueno...
        datosOk=true;
        if (datosOk){
            Comentario c = new Comentario(comentarios, puntuacion, idpistas, idusuarios);
            ComentarioController.guardar(c);
            // nos vamos a mostrar la lista, que ya contendrá el nuevo curso
            response.sendRedirect("/padelunion/pista/listpistas.jsp");
        }
    
    }
    //else
    }

%>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Padel Union App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="container">


<form action="#" method="POST">
    <fieldset>
    <legend class="text-center headerUserEdit">Nuevo comentario</legend>

    <div class="form-row">

        <div class="form-group col-md-4"></div>
    
        <div class="form-group col-md-4">
            <div class="form-group">
                <textarea name="comentarios"  type="textarea" class="form-control" id="comentarioInput"></textarea>
            </div>
        </div>

        <div class="form-group col-md-4"></div>

    </div>

    <div class="form-row">     

        <div class="form-group col-md-4"></div>

        <div class="form-group col-md-4">
            <div class="form-group">
<<<<<<< HEAD
=======

>>>>>>> e559e4e795727b779b210c58ac68fb36a7ae6b44
                <h6>Puntuación: <span id="demo"></span></h6>
                <input  name="puntuacion" type="range" class="custom-range" min="0" max="5" id="myRange">
                <div class="separador20"></div>
                
            </div>
        </div>

        <div class="form-group col-md-4"></div>

    </div>
  
        <input type="hidden" name="idpistas" value="<%= p.getId()%>"> 

    <div class="form-row">
   
        <div class="col-md-12 text-center">
          <button type="submit" class="btn btn-primary">Enviar comentario</button>   
        </div>
    
    </div> 

    </fieldset>
    </form>
  
</div>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>    

    <script>
        var slider = document.getElementById("myRange");
        var output = document.getElementById("demo");
        output.innerHTML = slider.value;

        slider.oninput = function() {
        output.innerHTML = this.value;
        }
    </script>

</body>
</html>