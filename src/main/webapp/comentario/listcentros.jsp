<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="container">

<div class="row">
<div class="col">
<h3>Centros</h3>
</div>
</div>


<div class="container">
            
            
            
               <% for (Centro c : CentroController.getAll()) { %>
                 <div class="row align-items-center">      
                    <div class="col-md-4">
                        
                        <img class="imgcentro" src="/padelunion/img/imgma/<%= c.getFoto() %>">
                        
                      </div> 

                    <div class="col-md-6">
                        <div><h5> <%= c.getNombre() %> </h5></div>
                        <div><span><%= c.getUbicacion() %></span></div>
                          <hr size="30" />
                        <div><span><i class="fas fa-phone-square"></i> <%= c.getTelefono() %></span></div>
                        <div><span><i class="fas fa-envelope-square"></i> <%= c.getEmail() %></span></div>
                        <hr size="30" />
                        <div><span> <%= c.getUrl() %></span></div>
                        

                      </div>  


                      <div class="col-md-2">
                        <span>Cantidad de pistas: </span>
                      </div>  
                                            
                        
                </div>
                <div class="separador50"></div>
              <%}%>                                 
            

    </div>
    



    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script> 

</body>
</html>
