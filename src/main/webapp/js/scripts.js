
 




$(document).on("click", "#envioModalLogin", function (e) {
    e.preventDefault();

    if ($("#login-pill").hasClass("active")) {
        $("#loginform").submit();
    } else {
        if ($("#reg-contrasena1").val() == $("#reg-contrasena2").val()) {
            $("#registroform").submit();
        } else {
            $("#contrasenaerror").show();
            console.log("¡Las contraseñas no coinciden!");
        }
    }

});

$(document).on("click", "#logoutFormLaunch", function (e) {
    e.preventDefault();
    //$("#logoutForm").submit();
    window.location.href = "/padelunion/logout.jsp";
});

var loginId =  $("#idloginId").val();

function copiaDeListaAModal(este){
    var caja = $(este).closest("div.caja-partido");
    var fecha = caja.find(".dato-fecha").html();
    var hora = caja.find(".dato-hora").html();
    var lugar = caja.find(".dato-lugar").html();
    var nivel = caja.find(".dato-nivel").html();
    var htmlBoton1 = caja.find(".btn1").html();
    var valorBoton1 = caja.find(".btn1").text();
    var htmlBoton2 = caja.find(".btn2").html();
    var valorBoton2 = caja.find(".btn2").text();
    var htmlBoton3 = caja.find(".btn3").html();
    var valorBoton3 = caja.find(".btn3").text();
    var htmlBoton4 = caja.find(".btn4").html();
    var valorBoton4 = caja.find(".btn4").text();
    var estado = caja.find(".dato-estado").html();   
    var idjugador1 = caja.find(".dato-idjugador1").val();
    var idjugador2 = caja.find(".dato-idjugador2").val();
    var idjugador3 = caja.find(".dato-idjugador3").val();
    var idjugador4 = caja.find(".dato-idjugador4").val();
    var idpartido = caja.find(".dato-idpartido").val(); 
    var idloginId = caja.find(".dato-idlogin").val(); 

    var modal = $("#Modalpartido");


    modal.find(".dato-fecha").html(fecha);
    modal.find(".dato-hora").html(hora);
    modal.find(".dato-lugar").html(lugar);
    modal.find(".dato-nivel").html(nivel);
    modal.find(".dato-idjugador1").val(idjugador1);
    modal.find(".dato-idjugador2").val(idjugador2);
    modal.find(".dato-idjugador3").val(idjugador3);
    modal.find(".dato-idjugador4").val(idjugador4);
    modal.find(".dato-idpartido").val(idpartido);
    modal.find(".dato-idlogin").val(idloginId);

    modal.find(".btn1modal").html(htmlBoton1);
    if(valorBoton1>0){
        $(".btn1modal").addClass("ocupado")
        
    } else {
        $(".btn1modal").removeClass("ocupado");
       
    }

    modal.find(".btn2modal").html(htmlBoton2);
    if(valorBoton2>0){
        $(".btn2modal").addClass("ocupado")
    } else {
        $(".btn2modal").removeClass("ocupado"); 
      
    }

    modal.find(".btn3modal").html(htmlBoton3);
    if(valorBoton3>0){
        $(".btn3modal").addClass("ocupado")
        
    } else {
        $(".btn3modal").removeClass("ocupado"); 
        
    }

    modal.find(".btn4modal").html(htmlBoton4);
    if(valorBoton4>0){
        $(".btn4modal").addClass("ocupado")
    } else {
        $(".btn4modal").removeClass("ocupado");
         
    }

    modal.find(".dato-estado").html(estado);
}

$(document).on("click", ".detalle-partido", function(){
    copiaDeListaAModal(this);
});

$(document).on("click", ".btn1modal", function(){


    if($("#idjugador1").val()==loginId){
        $(this).toggleClass("ocupado");
        document.getElementById("idjugador1").value=0;
        
    }else if($("#idjugador1").val()>0) {
        
        return;
    }else {
        
    $(this).toggleClass("ocupado");
         if($("#idjugador1").val()==0){
        document.getElementById("idjugador1").value=loginId;
       
        }
    }
    
});

$(document).on("click", ".btn2modal", function(){
    
    if($("#idjugador2").val()==loginId){
        $(this).toggleClass("ocupado");
        document.getElementById("idjugador2").value=0;
        
    }else if($("#idjugador2").val()>0) {
        
        return;
    }else {
        
    $(this).toggleClass("ocupado");
         if($("#idjugador2").val()==0){
        document.getElementById("idjugador2").value=loginId;
       
        }
    }

});

$(document).on("click", ".btn3modal", function(){
    
    if($("#idjugador3").val()==loginId){
        $(this).toggleClass("ocupado");
        document.getElementById("idjugador3").value=0;
        
    }else if($("#idjugador3").val()>0) {
        
        return;
    }else {
        
    $(this).toggleClass("ocupado");
         if($("#idjugador3").val()==0){
        document.getElementById("idjugador3").value=loginId;
       
        }
    }
    
});

$(document).on("click", ".btn4modal", function(){
   
    if($("#idjugador4").val()==loginId){
        $(this).toggleClass("ocupado");
        document.getElementById("idjugador4").value=0;
    
    }else if($("#idjugador4").val()>0) {
  
        return;
    }else {

    $(this).toggleClass("ocupado");
         if($("#idjugador4").val()==0){
        document.getElementById("idjugador4").value=loginId;
       
        }
    }

});

// $(document).on("click", "#idjugador1", function(){
   
//     if ($(".btn1modal").hasClass('ocupado')){
//         $("#idjugador1").val(33); 
//     }else {
//         $("#idjugador1").val(33);
//     }

// });

// $(document).on("click", "#btn", function(){
//     $("#Modalpartido").modal();
// });

$(document).ready(function(){
    $(document).on("click",".btnpp", function(){
        $("#Modalpartido").modal();
        copiaDeListaAModal(this);

    });
});


//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////

    var listaPartidos;

    $(document).ready(function(){
            
            $.getJSON("/padelunion/partido/listjson.jsp", function (data) {
                    listaPartidos=data;
                    pintaDatos();
            });

    });


    $(document).on("input",".filtrar", function(){
            pintaDatos();
    });

    $(document).on("input",".filtrarMisPartidos", function(){
        pintaDatos();
    });
  
    // $(document).on("input","#fechaPartidos", function(){
    //         pintaDatos();
    // });

    function horario(valor){
    
    if(!valor) return -1;

    var resultadoHora = 0;
    valor = valor.substr(0,2);
    var horaPartido = parseInt(valor);

    if(horaPartido>=8 && horaPartido<=14){
        resultadoHora = 0;
    } else if(horaPartido>14 && horaPartido<=21){
        resultadoHora = 1;
    } else{
        resultadoHora = 2;
    }

    return  resultadoHora;

    }

    function pintaDatos(){
    //cada vez que filtramos borramos lo que hay
    $('#marca').html("");

    function cambioDeFormato(fecha){
        var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        var todayTime = new Date(fecha);
        var month = meses[todayTime.getMonth()];
        var day = todayTime.getDate();
        return day + " de " + month;
        
    }

    listaPartidos.forEach(function (partido) {

            var mostrarPartido=true;

            if ($("#soloMisPartidos").length) {
                var misPartidosActual = $("#soloMisPartidos").val();
             
                if( partido.jugador1!=misPartidosActual 
                    && partido.jugador2!=misPartidosActual
                    && partido.jugador3!=misPartidosActual 
                    && partido.jugador4!=misPartidosActual
                    && partido.idcreador!=misPartidosActual) {
                             mostrarPartido=false; 
            }
        }



            // var fechaActual = $("#fechaPartidos").val();
            // if (partido.dia!=fechaActual) {
            //          mostrarPartido=false; 
            // }

            // var misPartidosActual = $("#FiltrarMisPartidos").val();
            //     if( partido.jugador1!=misPartidosActual 
            // || partido.jugador2!=misPartidosActual
            // || partido.jugador3!=misPartidosActual 
            // || partido.jugador4!=misPartidosActual
            // || partido.idcreador!=misPartidoActual) {
            //          mostrarPartido=false; 
            // } 

            var nivelActual = $("#NivelFiltro").val();
            if(nivelActual!=0 && partido.nivel!=nivelActual) {
                     mostrarPartido=false; 
            } 

            var fechaActual = $("#fechaPartidos").val();
            if(fechaActual!="" &&  partido.dia!=fechaActual) {
                     mostrarPartido=false; 
            };

            var estadoPartido = $("#EstadoPartido").val();
            if(estadoPartido!=3 && partido.estado!=estadoPartido){          
                     mostrarPartido=false; 
            }

            var horaPartido = $("#HoraPartidos").val();
            if(horaPartido!=3 && horario(partido.hora) != horaPartido) {
                     mostrarPartido=false; 
            }


            if (mostrarPartido==true){

            var textohtml = "";
            textohtml += '<div class="col-md-6 col-lg-4">';
            textohtml += '<div class="caja-partido alert alert-danger" role="alert" style="width:100%;">';
            textohtml += '<div class="paradia dato-fecha"><b>'+ cambioDeFormato(partido.dia) +'</b></div>';
            // textohtml += '<div class="dato-email">Email: '+ partido.idcreador +'</div>';
            textohtml += '<div class="paradia dato-hora"><b>Horario:</b> '+ partido.hora +'</div>';
            textohtml += '<div class="paradia dato-lugar"><b>Localidad:</b> '+ partido.sitio +'</div>';
            textohtml += '<hr size="10" />';
            textohtml += '<div class="paradia dato-nivel"><b>Nivel:</b> '+ nivelescrito(partido.nivel) +'</div>';
            textohtml += '<hr size="10" />';
            textohtml += '<div class="pistapadel">';
            textohtml += '<img src="/padelunion/img/pistapadel.jpg" class="img-fluid">';
            textohtml += '<button type="button" class="btnpp btnx btn1 ' + ((partido.jugador1>0) ? "ocupado" : "") +' "> <div class="ocultatexto">' +  partido.jugador1 +'</div></button>';
            textohtml += '<button type="button" class="btnpp btnx btn2 ' + ((partido.jugador2>0) ? "ocupado" : "") +' "> <div class="ocultatexto">' +  partido.jugador2 +'</div></button>';
            textohtml += '<button type="button" class="btnpp btnx btn3 ' + ((partido.jugador3>0) ? "ocupado" : "") +' "> <div class="ocultatexto">' +  partido.jugador3 +'</div></button>';
            textohtml += '<button type="button" class="btnpp btnx btn4 ' + ((partido.jugador4>0) ? "ocupado" : "") +' "> <div class="ocultatexto">' +  partido.jugador4 +'</div></button>';
            textohtml += '</div>';
            textohtml += '<hr size="10" />';
            textohtml += '<div class="paradia dato-estado"><b>Estado:</b>';            

            if(partido.estado==0){textohtml += ' Abierto'} 
            else if(partido.estado==1){textohtml += ' En proceso'}
            else{textohtml += ' Finalizado'};

            textohtml += '</div>';
            textohtml += '<hr size="10" />';
            textohtml += '<input type="hidden" class="dato-idjugador1"  value="' +  partido.jugador1 +'"/>';
            textohtml += '<input type="hidden" class="dato-idjugador2"  value="' +  partido.jugador2 +'"/>';
            textohtml += '<input type="hidden" class="dato-idjugador3"  value="' +  partido.jugador3 +'"/>';
            textohtml += '<input type="hidden" class="dato-idjugador4"  value="' +  partido.jugador4 +'"/>';
            textohtml += '<input class="dato-idpartido" type="hidden" value="' + partido.idpartidos + '">';
            textohtml += '<button type="button" class="nav-link detalle-partido btn btn-danger" href="#" data-id='+ partido.idpartidos +' data-toggle="modal" data-target="#Modalpartido">';
            textohtml += 'Jugar';
            textohtml += '</button>';
            
            textohtml += '</div>';
            textohtml += '</div>';

            //Esto incrusta el texto dentro del div cuyo id sea marca
            $('#marca').append(textohtml);

            }


            });
    }


    $(document).on("click", "#publicarPartido", function () {
        
        //idpartido, jugadores y estado con ajax
        //Post en click tras publicar. direccion a jsp que le envie el paquete de datos 
        //y desde alli los envia
            var modal = $("#Modalpartido");

         
            var idpartidos = $("#idpartidomodal").val();

            
            //var idpartido = $(".dato-idpartido").val();
           
            if ($(".btn1modal").hasClass('ocupado')){
               
               var jug1 = $("#idjugador1").val();
               
            }else {
             
                var jug1 = 0;
            }
            if ($(".btn2modal").hasClass('ocupado')){
                var jug2 = $("#idjugador2").val();
            }else {
                var jug2 = 0;
            }
            if ($(".btn3modal").hasClass('ocupado')){
                var jug3 = $("#idjugador3").val();
            }else {
                var jug3 = 0;
            }
            if ($(".btn4modal").hasClass('ocupado')){
                var jug4 = $("#idjugador4").val();
            }else {
                var jug4 = 0;
            }

            if(jug1>0 && jug2>0 && jug3>0 && jug4>0){
                var estado = 1;
            }else {
                var estado = 0;
            }

            var data = {idpartidos,jug1,jug2,jug3,jug4,estado};

          
            $.post({
                url: '/padelunion/partido/jsonPartido.jsp',
                data: data,
               
                success: function(response){
                    console.log("respuesta "+response);
                    if (response.indexOf("ok")>-1) {
                        console.log("todo bien");
                    } else {
                        console.log("algo falla");
                    }
                },
                error: function(){
                    console.log("error no respuesta");
                }
            }
            );

            location.reload(true);

        // Enviar partido para guardar $("").submit();
    });
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////

$(document).on("input",".filtrarPistas", function(){
    pintaDatosPistas();
});

var listaPistas;
var textohtml ;

$(document).ready(function(){
        
        $.getJSON("/padelunion/pista/listjson.jsp", function (data) {
            listaPistas=data;
                pintaDatosPistas();
        });

});


function nivelescrito(n){

    var nivelescrito;
    
    switch(n){

        case  1:
                nivelescrito = "Iniciación";
                 break;
        case  2:
                nivelescrito = "Intermedio";
                 break;
        case  3:
                 nivelescrito = "Intermedio alto";
                 break;
        case 4:
                nivelescrito = "Avanzado";
                 break;
        case 5:
                nivelescrito = "Competición";
                 break;
        case 6:
                nivelescrito = "Profesional";
                 break;
        default: 
                nivelescrito = "Profesional Máximo";

            }

        return nivelescrito;

    
}

function pintaDatosPistas(){
    //cada vez que filtramos borramos lo que hay
    $('#marcaPistas').html("");
    console.log("pintaDatos");

    listaPistas.forEach(function (pista) {
        console.log(pista);
            var mostrarPista=true;

            var centroActual = $("#centroFiltro").val();
            if(centroActual!=0 && pista.idcentros!=centroActual) {
                mostrarPista=false; 
            }
            console.log("centro actual "+centroActual);
            console.log("centro pista "+pista.idcentros);

            if ( mostrarPista==true){                            

            textohtml = "";

            textohtml += '<tr>';
            textohtml += '<td scope="row">'+ pista.nombre +'</td>';
            textohtml += '<td>';

            if(pista.tipo==0){textohtml += 'Cemento';}
            else if (pista.tipo==1){textohtml += 'Moqueta';}
            else{textohtml += 'Otros';}

            textohtml += '</td>';
            textohtml += '<td>';

            if(pista.indoor==0){textohtml += 'Outdoor';}
            else{textohtml += 'Indoor';}

            textohtml += '</td>';
            if(pista.numcomentarios==0){
            textohtml += '<td><i class="fas fa-comment-dots"></i></td>';
            } else{
            textohtml += '<td><a href="/padelunion/comentario/list.jsp?idpista='+ pista.idpistas +'">' + pista.numcomentarios +' <i class="fas fa-comment-dots"></i></a></td>';
            }

            textohtml += '<td><a href="/padelunion/comentario/create.jsp?id='+ pista.idpistas+'">Añadir</a></td>';
            textohtml += '</tr>';
              
            //Esto incrusta el texto dentro del div cuyo id sea marcaPistas
            $('#marcaPistas').append(textohtml);


            }

            });
    }
