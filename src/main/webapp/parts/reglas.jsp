<%@ page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosdavid.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosangeles.css">
</head>
<body>
<center>
<!-- carrusel de imagenes -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="../img/padel1.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
       <img class="d-block w-100" src="../img/padel2.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
       <img class="d-block w-100" src="../img/padel3.jpg" alt="First slide">

    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

</center>
<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>
<div class="container">


<div class="text-justify alert alert-info" role="alert">
<h2 class="text-center">Reglas resumidas del pádel</h2>

<h3>Número de jugadores</h3>
<p>Es un deporte de equipos, donde son cuatro los jugadores. Se juega por parejas.</p>

<h3>El Juego</h3>  
<li>El pádel se puntúa como el tenis. Se juegan 3 sets. Los juegos siguen el tanteo de 15, 30, 40, Ventajas y/o Iguales.</li>
<li>Cuando se finalizan 3 juegos, se cambia de pista. </li>
<li>La bola puede ser golpeada por cualquier jugador y en cualquier sitio de la pista. </li>
<li>En el pádel, para que la bola se considere buena, debe tocar el suelo antes de impactar con las paredes y/o verja tras botar en el suelo, excepto en el servicio. Durante el transcurso de un punto la bola puede golpear en paredes y verja. El jugador puede golpear la bola con o sin bote previo pero cumpliendo el máximo de un bote para golpearla. </li>
<li>¿Cuándo es bueno el saque y como se efectúa? Esta es una de las reglas del pádel más importantes, el saque se considera bueno si la bola bota dentro del cuadro correspondiente tras ser golpeada por el jugador al servicio. Al igual que en el tenis el saque debe ser cruzado, es decir, el jugador que sirve desde la mitad derecha de su pista debe dirigir el mismo hacia el cuadrado a su izquierda de la pista rival. A diferencia del tenis, en el pádel no se lanza la bola por encima de la cabeza para realizar el servicio. En el pádel el jugador que realiza el saque deja caer la bola a un lado del cuerpo y la golpeará sin que la pala supere la altura de la cintura el momento del impacto. El saque genera dudas cuando la bola golpea en la verja lateral. El saque puede dirigirse a cualquier zona del cuadro rival pero nunca puede golpear verja lateral tras el bote. En cada punto que jugamos al servicio tenemos dos oportunidades para poner la bola en juego.</li>
<li>Y cuando la saco y la bola toca la red y pasa al otro lado de la pista? Si la bola tras el servicio toca la red y pasa al otro lado de la pista se pueden dar varias situaciones:
    <ul>
      <li> Si tras tocar la red la bola no bota en el cuadro correspondiente será mala siempre.</li>
      <li>Si tras tocar la red la bola bota en el cuadro correspondiente:</li>
        <ul>
          <li> La bola bota una vez y no toca verja lateral: Se considera nula y el jugador tiene derecho a repetir ese servicio.</li>
          <li> La bola bota dos veces toca verja lateral: Se considera nula y el jugador tiene derecho a repetir ese servicio.</li>
          <li> La bola bota una vez toca verja lateral: Se considera mala  y el jugador no tiene derecho a repetir ese servicio.</li>
          </ul>
    </ul>
  <li> Si la bola me roza/golpea tras el envío del rival o un rebote en paredes/verjas el punto lo gana el rival. </li>
  <li> En ocasiones y de manera accidental la bola da dos toques en mi pala antes de enviarla al otro lado de la pista. En ese caso el punto lo he perdido.</li>
  <li> Nunca puedo golpear la bola si la pala no está sujeta por mi mano y tampoco está permitido jugar competición sin que la pala este sujeta por el cordón que tiene al final de la empuñadura. Es muy importante acostumbrarse a jugar desde el primer día con el cordón de la pala alrededor de la muñeca. Es una medida de seguridad muy importante.</li>
  <li>Si el rival golpea la bola con fuerza y:  </li>
      <ul>
        <li> La bola bota en la pista y sale fuera de esta. El punto se considera ganado por el jugador que la golpeo aunque tenemos la posibilidad de salir y golpearla hacia adentro de la pista y al campo rival antes de el segundo bote.</li>
        <li> Bota, toca la pared/paredes y/o verja y vuelve hacia el campo rival. El punto se considera ganado por el jugador que la golpeo aunque tenemos la opción de golpearla incluso cuando haya superado la línea de la red hacia su pista.</li>
      </ul> 
  <li> Si tocamos la red con mi pala, ropa o cualquier parte del cuerpo tras golpear la bola, el punto lo gana mi rival.</li>
  <li>Cómo se si una bola que bota justo en la unión entre el suelo y las paredes/verjas?. Las reglas del pádel dicen que una bola que en esta situación bota con un ángulo de 45 grados se considera buena, pero nuestro consejo es que en estas situaciones no debemos discutir con nuestro rival. Todo se arregla repitiendo el punto. </li>
<p></p>
</div>

<div class="alert alert-warning" valign="middle" role="alert">
<p>Para más información puedes ver el manual de la Federacion Española de Pádel <a target="_blank" href="http://www.padelfederacion.es/Paginas/docs/REGLAMENTOJUEGO2010.pdf"> aquí.</a> </p>
<a href="http://www.padelfederacion.es/Paginas/docs/REGLAMENTOJUEGO2010.pdf"> 
</div>

<div class="separador50"></div>

</div>
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>


</body>
</html>
