<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>


<center>
<div class="container">
        <form action="#" method="POST">

            <input type="hidden" name="filtrosform" value="filtrosform">

            <div class="row align-items-center">
                <div class="col-12 col-md-2">
                        
                </div>

                <div class="col-12 col-md-3">
                <h5>Día:</h5>
                </div>

                <div class="col-12 col-md-2">
                <h5>Horario:</h5> 
                </div>

                <div class="col-12 col-md-2">
                <h5>Nivel:</h5> 
                </div>

                <div class="col-12 col-md-2">
                <h5>Estado:</h5> 
                </div>

            </div>

            <div class="row align-items-center">

                <div class="col-12 col-md-2">
                        <%-- <button type="button" id="cargajson" class="btn btn-sm btn-danger">Cargar partidos</button> --%>
                </div>

                <div class="col-12 col-md-3">
                  
                        <%-- <label>Dia</label> --%>
                        <input class="form-control filtrar" type="date"  id="fechaPartidos" value="">
                 
                </div>
                <div class="col-12 col-md-2">
                        <%-- <label>Horario</label> --%>
                        <select name="horario" type="time" class="form-control filtrar" id="HoraPartidos">
                                <option selected value="3">-</option>
                                <option value="0">Mañana</option>
                                <option value="1">Tarde</option> 
                                <option value="2">Noche</option>
         
                        </select>
                </div>
                <div class="col-12 col-md-2">
                          <%-- <label>Nivel</label> --%>
                        <select  name="nivelfiltro" type="text" class="form-control filtrar" id="NivelFiltro">
                                <option selected value="0">-</option>
                                <option value="1">Iniciación</option>
                                <option value="2">Intermedio</option>      
                                <option value="3">Intermedio alto</option>
                                <option value="4">Avanzado</option>
                                <option value="5">Competición</option>
                                <option value="6">Profesional</option>      
                                <option value="7">Profesional máximo</option>
                
                        </select>
                </div>
                <div class="col-12 col-md-2">
                  
                        <%-- <label>Estado</label> --%>
                        <select class="form-control" type="text" class="form-control filtrar" id="EstadoPartido" >
                                <option selected value="3">-</option>
                                <option value="0">Abierto</option>
                                <option value="1">En proceso</option>      
                                <option value="2">Finalizado</option>
                        </select>
                </div>
                
            </div>                     
        </form> 
</div>      
</center>

<input type="hidden" class="filtrar" id="FiltrarMisPartidos" value="<%= loginId%>">
<div class="separador50"></div>


<%-- Modal para el partido --%>
<%@include file="/partido/modalpartido.jsp"%>

<div class="container">
<center>
        <div class="row align-items-center" id="marca">

        </div>
</center>
        
</div>