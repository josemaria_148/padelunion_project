<%@ page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>
<%

  //inicializamos las variables loginId y loginName, por el momento sin valor
  int loginId=0;
  String loginName=null;


  if (session.getAttribute("loginId") != null) {

          loginId = (Integer) session.getAttribute("loginId");
          if (loginId>0){
            loginName = (String) session.getAttribute("loginName");
          }

      }
    int admin = UsuarioController.EresAdmin(loginId);

%>

  <!--Diseño de la página web. Definición de los menús desplegables-->
  <nav id="menuDesplegable" class="navbar navbar-expand-lg  navbar-dark bg-primary">

    <%-- <a class="push_buttonimagennav"><img class="logo" src="/padelunion/img/imgma/LOGO2.jpg"></a> --%>

    <a class="navbar-brand" href="/padelunion">Inicio</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">

        <li class="nav-item dropdown">
          <a class="nav-link" href="/padelunion/parts/preprincipal.jsp">Menú principal  </a>                  
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            Información
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/padelunion/calculanivel/funcionamiento.jsp">Niveles</a>
            <a class="dropdown-item" href="/padelunion/parts/reglas.jsp">Reglas de juego</a>
            <a class="dropdown-item" href="/padelunion/parts/aprende.jsp">Aprende y diviértete</a>
            <!--<a class="dropdown-item" href="">Pista de la ciudad</a>-->
            <a class="dropdown-item" href="/padelunion/parts/objetivo.jsp">Objetivo de la aplicación</a>

          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            Tablón de anuncios
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/padelunion/parts/tiendas.jsp">Oferta de material deportivo</a>
            <a class="dropdown-item" href="/padelunion/parts/cartelestorneos.jsp">Carteles de torneos</a>
            <a class="dropdown-item" href="/padelunion/parts/foro.jsp">Foro</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            ¿Quieres conocer tu nivel?
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="/padelunion/calculanivel/edit.jsp">Calcula tu nivel</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <% if (loginId>0) { %>
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            Partidos
          </a>
          <% } %>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
         
          <% if(admin==1){%>
            
            <a class="dropdown-item" href="/padelunion/partido/list.jsp">Listado</a>
            <a class="dropdown-item" href="/padelunion/partido/mispartidos.jsp">Mis Partidos</a>
            <a class="dropdown-item" href="/padelunion/partido/create.jsp">Nuevo Partido</a>
         
            <%} else{%>
            <a class="dropdown-item" href="/padelunion/partido/create.jsp">Nuevo Partido</a>
            <a class="dropdown-item" href="/padelunion/partido/mispartidos.jsp">Mis Partidos</a>
            <%}%>
          </div>
        </li>

        <li class="nav-item dropdown">
          
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            Centros
          </a>
          
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            
             <% if(admin==1){%>
            <a class="dropdown-item" href="/padelunion/centro/list.jsp">Listado</a>
            <a class="dropdown-item" href="/padelunion/centro/create.jsp">Nuevo Centro</a>
            <%} else{%>
            <a class="dropdown-item" href="/padelunion/centro/listcentros.jsp">Listar Centros</a>
              <% } %>
          </div>
        </li>

        <li class="nav-item dropdown">
          <% if (loginId>0) { %>
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            Pistas
          </a>
          <% } %>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">

           <% if(admin==1){%>
            <a class="dropdown-item" href="/padelunion/pista/listpistas.jsp">Listado</a>
            <a class="dropdown-item" href="/padelunion/pista/create.jsp">Nueva Pista</a>
            <%} else{%>
            <a class="dropdown-item" href="/padelunion/pista/listpistas.jsp">Listar Pistas</a>
            <% } %>
          </div>
        </li>

        <li class="nav-item dropdown">
          <% if (loginId>0) { %>
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Usuarios
          </a>
          <% } %>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
             <% if(admin==1){%>
              <a class="dropdown-item" href="/padelunion/usuario/list.jsp">Listado</a>
              <% } else{%>
              <a class="dropdown-item" href="/padelunion/usuario/editusuario.jsp">Modificar datos</a>
              <% } %>
          </div>
        </li>

      </ul>

      <ul class="navbar-nav">
        <li class="nav-item">
            <% if (loginId<1) { %>
                <a class="nav-link" href="#" data-toggle="modal" data-target="#dobleModal">
                Login
                </a>
            <% } else { %>

            <%-- <form action="/padelunion/logout.jsp" method="POST" id="logoutForm">
                <input type="hidden" name="logoutpost" value="modal"> --%>
                <a class="nav-link" id="logoutFormLaunch" href="#">Logout <%= loginName %></a> 
           <%--   </form> --%>

            <% } %>
        </li>
      </ul>

    </div>

    <%-- <div align="right">
      <div class="collapse navbar-collapse" id="navbarSupportedContent1">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item dropdown">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#dobleModal" data-whatever="login">Log in</button>
          </li>
        </ul>
      </div>
    </div> --%>
  </nav>