<%@ page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosdavid.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosangeles.css">
</head>
<body>
<center>
<!-- carrusel de imagenes -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="../img/padel1.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
       <img class="d-block w-100" src="../img/padel2.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
       <img class="d-block w-100" src="../img/padel3.jpg" alt="First slide">

    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

</center>
<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>


<div class="container" >

<div class="text-center alert alert-danger" valign="middle">  
<h2>Torneos y ligas en la ciudad y provincia de Salamanca</h2>
</div>

<div class="separador50"></div>

  <div class="row " >

      <div class="col-md-1"></div>      

      <div class="text-center alert alert-info col-12 col-sm-6 col-md-4">  
         <h5> Liga Club Padel Home </h5>
          <a target="_blank" href="http://www.clubpadelhome.com/concurso">
            <img class="cartel" src="../img/ligastorneos/clubpadelhome0708.jpg">
          </a>   
     </div>

    <div class="col-md-2"></div>      
     
    <div class="text-center alert alert-info col-12 col-sm-6 col-md-4">  
          <h5> Liga Padel You </h5>
          <a target="_blank" href="http://www.padelyou.com/salamanca/?page_id=256">
            <img class="carteltorneopadel" src="../img/ligastorneos/padelyou.jpg">
          </a>
    </div>

    <div class="col-md-1"></div>      
     
     
  </div>
</div>

<div class="separador50"></div>


  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>


</body>
</html>