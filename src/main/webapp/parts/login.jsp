<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>
<%@page import="java.util.*" %>

<%
/*
LOGIN.JSP - Módulo "incrustable"
contiene menú modal donde se solicta el nombre de usuario o email y el password
el form envía via post el resultado al JSP el codigo siguiente procesa los datos e intenta el login
en caso que lo consiga, asigna las variables de sesión correspondientes
este LOGIN.JSP debe incrustarse ANTES del MENU.JSP
*/

//System.out.println(request.getContextPath());

// si recibimos datos via POST
if ("POST".equalsIgnoreCase(request.getMethod())) {
    request.setCharacterEncoding("UTF-8");

    //miramos si existe el parámetro "loginpost"
    String login = request.getParameter("loginpost");
    if (login!=null){
        String paramName = request.getParameter("loginemail");
        String paramPassword = request.getParameter("loginpassword");
        //aquí ejecutamos el método que debe verificar nombre y password
        //recibiremos en itemId un id válido >0 o bien -1 (VER METODO)
        int itemId = UsuarioController.checkLogin(paramName,paramPassword);
        //si hemos recibido id válido, asignamos variables de session
        if (itemId>0) {
            //HttpSession session=request.getSession();
            session.setAttribute("loginName",paramName);
            session.setAttribute("loginId",itemId);
        }
    }

    //miramos si existe el parámetro "logoutpost"
    /*
    String logout = request.getParameter("logoutpost");
    if (logout!=null){
        session.setAttribute("loginId",0);
        session.setAttribute("loginName","");
        response.sendRedirect("/padelunion/index.jsp");
        return;
    }
  */
} 

//Esto es para el registro

boolean datosOk2;

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo alumno
    
        //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
        request.setCharacterEncoding("UTF-8");

    String register = request.getParameter("registroform");
    if (register!=null){

        String nombre = request.getParameter("nombre");
        String apellidos = request.getParameter("apellidos");
        String email = request.getParameter("email");
        String clave = request.getParameter("pass"); 
        Date fechanacimiento =  MiLib.stringToDate(request.getParameter("fechanacimiento"));
        int Nivel = Integer.parseInt(request.getParameter("nivel")); 
        int Mano =  Integer.parseInt(request.getParameter("mano")); 
        int Sexo = Integer.parseInt(request.getParameter("sexo"));       
        int administrador = Integer.parseInt(request.getParameter("administrador"));
        String telefono = request.getParameter("telefono");
        
        // aquí verificaríamos que todo ok, y solo si todo ok hacemos guardar, 
        // por el momento lo damos por bueno...
        datosOk2=true;
        if (datosOk2){
            Usuario usu = new Usuario( nombre, apellidos, email,  clave, fechanacimiento, Nivel, Mano,
         Sexo,  administrador,  telefono);
            UsuarioController.guardar(usu);
            // nos vamos a mostrar la lista, que ya contendrá el nuevo curso
            response.sendRedirect("/padelunion/index.jsp");
        }
    }
    }

%>

  
<!-- Modal con form login -->
<%-- <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="loginModalLabel">Identificación de usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="#" method="POST">
        <div class="modal-body">
                <input name="loginname" type="text" class="form-control"  placeholder="email">
                <input name="loginpassword" type="password" class="form-control" placeholder="password">
                <input type="hidden" name="loginpost" value="modal">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
      </form>
    </div>
  </div>
</div> --%>

<div class="modal fade" id="dobleModal" tabindex="-1" role="dialog" aria-labelledby="dobleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">

          <ul class="nav nav-pills " id="pills-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="login-pill-tab" data-toggle="pill" href="#login-pill" role="tab" aria-controls="login-pill"
                aria-selected="true">Login</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="registro-pill-tab" data-toggle="pill" href="#registro-pill" role="tab" aria-controls="registro-pill"
                aria-selected="false">Registro</a>
            </li>
          </ul>

          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        
        <div class="modal-body">



          <div class="tab-content" id="pills-tabContent">

            <div class="tab-pane fade show active" id="login-pill" role="tabpanel" aria-labelledby="login-pill-tab">

              <form id="loginform" method="POST" action="#">
                <div class="form-group">
                  <%-- <label for="login-email" class="col-form-label">Email:</label> --%>
                  <input name="loginemail" type="text" class="form-control" id="login-email" placeholder="Email">
                </div>
                <div class="form-group">
                    <%-- <label for="login-password" class="col-form-label">Contraseña:</label> --%>
                    <input name="loginpassword" type="password" class="form-control" id="login-password" placeholder="Contraseña"/>
                </div>
                <input type="hidden" name="loginpost" value="modal">
                <input type="hidden" name="loginform" value="loginform">
              </form>

            </div>

            <div class="tab-pane fade" id="registro-pill" role="tabpanel" aria-labelledby="registro-pill-tab">
            
              <form id="registroform" method="POST" action="#">
              <div class="form-row">
                <div class="form-group col-md-6">
                  <%-- <label for="reg-name" class="col-form-label">Nombre:</label> --%>
                  <input name="nombre" type="text" class="form-control" id="reg-name" placeholder="Nombre">
                </div>
                <div class="form-group col-md-6">
                    <%-- <label for="reg-apellidos" class="col-form-label">Apellidos:</label> --%>
                    <input name="apellidos" type="text" class="form-control" id="reg-apellidos" placeholder="Apellidos">
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                    <%-- <label for="reg-telefono" class="col-form-label">Teléfono:</label> --%>
                    <input name="telefono" type="text" class="form-control" id="reg-telefono" placeholder="Teléfono">
                </div>
                <div class="form-group col-md-6">
                    <%-- <label for="reg-email" class="col-form-label">Email:</label> --%>
                    <input name="email" type="email" class="form-control" id="reg-email" placeholder="Email">
                </div>
                </div>
                <div class="form-row">
                <div class="form-group col-md-6">
                  <%-- <label for="reg-contrasena1" class="col-form-label">Contraseña:</label> --%>
                  <input name="pass_confirmation" data-validation="length" data-validation-length="min8" id="reg-contrasena1" type="password" class="form-control" placeholder="Contraseña" />
                </div>
                <div class="form-group col-md-6">
                    <%-- <label for="reg-contrasena2" class="col-form-label">Verificar contraseña:</label> --%>
                    <input name="pass" data-validation="confirmation" id="reg-contrasena2" type="password" class="form-control" placeholder="Verificar contraseña"/>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="reg-nacimiento" class="col-form-label">Fecha de nacimiento</label>
                    <input name="fechanacimiento" type="date" class="form-control" id="reg-nacimiento">
                </div>
                <div class="form-group col-md-6">
                    <label for="reg-sexo" class="col-form-label">Sexo</label>
                    <select name="sexo" class="form-control" id="reg-sexo">
                        <option class="form-control" id="reg-sexoMasculino" value="0">Masculino</option>
                        <option class="form-control" id="reg-sexoFemenino" value="1">Femenino</option>
                    </select>
                </div>
              </div>
              <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="reg-mano" class="col-form-label">¿Eres diestro o zurdo?</label>
                      <select name="mano" class="form-control" id="reg-mano">
                        <option class="form-control" id="reg-manoDiestra" value="0">Diestro</option>
                        <option class="form-control" id="reg-manoZurda" value="1">Zurdo</option>
                      </select>
                </div>   
                <div class="form-group col-md-6">
                    <label for="reg-nivel" class="col-form-label">Nivel</label>
                    <select name="nivel" class="form-control" id="reg-nivel">
                        <option class="form-control" value="1">Iniciación</option>
                        <option class="form-control" value="2">Intermedio</option>
                        <option class="form-control" value="3">Intermedio alto</option>
                        <option class="form-control" value="4">Avanzado</option>
                        <option class="form-control" value="5">Competición</option>
                        <option class="form-control" value="6">Profesional</option>
                        <option class="form-control" value="7">Profesional máximo</option>
                    </select>
                </div>             
              </div>
                
                <input type="hidden" name="registroform" value="registroform">  
                <input type="hidden" name="administrador" value="0">  
              </form>
            </div>
            
          </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>        
            <button id="envioModalLogin" type="button" class="btn btn-primary">Enviar</button>
        </div>

      </div>
    </div>
</div>
