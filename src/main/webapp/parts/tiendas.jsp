<%@ page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosdavid.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosangeles.css">
</head>
<body>
<center>
<!-- carrusel de imagenes -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="../img/padel1.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
       <img class="d-block w-100" src="../img/padel2.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
       <img class="d-block w-100" src="../img/padel3.jpg" alt="First slide">

    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

</center>
<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>


<div class="container" >

<div class="text-center alert alert-danger" valign="middle">  
<h2> Tiendas de pádel en Salamanca y alrededores</h2>
</div>

<div class="separador50"></div>

  <div class="row" >
      <div class="text-center alert alert-info col-12 col-sm-6 col-md-4" valign="middle">  
         <h5>Tengo Tenis Salamanca</h5>
         <p> Calle Bolivia, 14, 37003 Salamanca </p>
          <a target="_blank" href="https://www.facebook.com/pages/Tengotenis-Salamanca/1144273672283906">
            <img class="cartel" src="../img/tiendas/tengotenis.jpg">
          </a>   
     </div>
    <div class="text-center alert alert-info col-12 col-sm-6 col-md-4">  
          <h5>Enforma Deportes</h5>
          <p>  Calle Azafranal, 21, 37001 Salamanca </p>
          <a target="_blank" href="https://www.facebook.com/EnformaDeporte/">
            <img class="cartel" src="../img/tiendas/enformasalamanca.jpg">
          </a>
     </div>
     <div class="text-center alert alert-info col-12 col-sm-6 col-md-4">  
          <h5>Más que Pádel Salamanca</h5>
          <p>  Polígono El Montalvo III, Calle Primera, 41, 37188 Carbajosa de la Sagrada, Salamanca </p>
          <a target="_blank" href="http://www.padelmastercenter.com/">
            <img class="cartel" src="../img/imgma/maspadel.jpg">
          </a>
     </div>
     <div class="text-center alert alert-info col-12 col-sm-6 col-md-4" valign="middle">  
          <h5> Padelmania Salamanca </h5>
          <p> Calle Valverdón, 2 Local bajo, 37006 Salamanca </p>
          <a target="_blank" href="https://www.facebook.com/padelmania.salamanca">
            <img class="cartelpadelmania" src="../img/tiendas/padelexpress.jpg">
          </a>
     </div>
     
     <div class="text-center alert alert-info col-12 col-sm-6 col-md-4">  
          <h5>Suministros Deportivos</h5>
          <p>  Calle Lepanto, 25, 37003 Salamanca </p>
          <a target="_blank" href="http://www.suministrosdeportivos.es/">
            <img class="cartel" src="../img/tiendas/suministrosdeportivos.png">
          </a>
     </div>
     <div class="text-center alert alert-info col-12 col-sm-6 col-md-4">  
          <h5>Padel Vip</h5>
          <p>  Tienda On Line </p>
          <a target="_blank" href="https://www.padelvip.com/">
            <img class="cartel" src="../img/tiendas/padelvip.jpg">
          </a>
     </div>
     <div class="text-center alert alert-info col-12 col-sm-6 col-md-4">  
          <h5>New Padel</h5>
          <p>  Tienda On Line </p>
          <a target="_blank" href="https://www.newpadel.es">
            <img class="cartel" src="../img/tiendas/newpadel.jpg">
          </a>
     </div>
  </div>
</div>

<div class="separador50"></div>


  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>


</body>
</html>