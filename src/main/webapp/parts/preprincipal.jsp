<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>

<!DOCTYPE html>

<html lang="es-ES">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>PadelUnion App</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
    crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
  <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
  <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
  <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosangeles.css">
  <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosprueba.css">
  

</head>

<body id="preprincipal">

<div class=separador50></div>

<div class="danger">

    <div class="container text-center">
      <div class="row">
        <div class="col">
          <a class="push_buttonimagen"><img class="logo" src="/padelunion/img/imgma/LOGO2.jpg"></a>
        </div>
      </div>

    <div class=separador50></div>
    <div class=separador50></div>


      <div class="row ">

        <div class="col">
        <a class="push_button green" href="/padelunion/index.jsp">
          <div class="margenIzquierda">    
          <img class="mr-3" src="/padelunion/img/pelota1.png" alt="Generic placeholder image">Jugar partido
          </div>
        </a> 
        </div>

      </div>

    <div class=separador50></div>
      

      <div class="row">

        <div class="col">
        <a class="push_button purple" href="/padelunion/calculanivel/edit.jsp">
        <div class="margenIzquierda75">
          <img class="mr-3" src="/padelunion/img/pelota4.png" alt="Generic placeholder image">Calcular nivel
          </div>
        </a> 
        </div>
      
        
      </div>

    <div class=separador50></div>

      <div class="row ">

        <div class="col">
        <a class="push_button red" href="/padelunion/parts/reglas.jsp">
          <div class="margenIzquierda40">
          <img class="mr-3" src="/padelunion/img/pelota3.png" alt="Generic placeholder image">Reglas de juego
          </div>
        </a> 
        </div>
        
      </div>

    <div class=separador50></div>

      <div class="row ">

        <div class="col">
        <a class="push_button blue" href="/padelunion/centro/listcentros.jsp">
          <img class="mr-3" src="/padelunion/img/pelota2.png" alt="Generic placeholder image">Centros deportivos
        </a> 
        </div>
        
      </div>


    <div class=separador50></div>
  
  </div>


  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ"
    crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm"
    crossorigin="anonymous"></script>
  <script src="/js/scripts.js"></script>


</body>

</html>