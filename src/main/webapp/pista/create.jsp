<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>
<%@page import="java.util.*" %>

<%
    boolean datosOk;

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
     //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
        request.setCharacterEncoding("UTF-8");
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo alumno
    String crear = request.getParameter("createForm");
    if (crear!=null){
       

         String createform = request.getParameter("createform");
         if (createform!=null){
  
             String nombre = request.getParameter("nombre");
             int tipo = Integer.parseInt(request.getParameter("tipo"));
             int indoor = Integer.parseInt(request.getParameter("indoor"));
             int idcentros = Integer.parseInt(request.getParameter("idcentros"));
        
        // aquí verificaríamos que todo ok, y solo si todo ok hacemos SAVE, 
        // por el momento lo damos por bueno...
              datosOk=true;
              if (datosOk){
                    Pista c = new Pista(nombre,tipo,indoor,idcentros);
                   PistaController.guardar(c);
                   // nos vamos a mostrar la lista, que ya contendrá el nuevo centro
                  response.sendRedirect("/padelunion/pista/list.jsp");
                    return;
              }
            } else {  
                response.sendRedirect("/padelunion/index.jsp");
                return;
                }
    }
    }

%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Padel Union App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>



<div class="container">
<div class="row">
<div class="col">
<h1>Nueva Pista</h1>
</div>
</div>


<div class="row">
<div class="col-md-8">

<form action="#" method="POST">
  <div class="form-group">
    <label for="nombreInput">Nombre de la pista</label>
    <input  name="nombre"  type="text" class="form-control" id="nombreInput" >
  </div>

   <div class="form-group">
    <label for="idcentroInput">Centro del curso</label>
    <select class="form-control" name="idcentros" type="text" class="form-control" id="idcentroInput" >
     <% for (Centro c : CentroController.getAll()){%>
      <option value="<%=c.getId()%>"><%= c.getNombre()%></option>
      
      <% }%>
    </select>
  </div>

   <div class="form-group">
    <label for="tipoInput">Tipo</label>
    <select  name="tipo"  type="text" class="form-control" id="tipo">
     <option value = "0">Cemento</option>
     <option value = "1">Moqueta</option>
     <option value = "2">Otro</option>
    </select>
  </div>

  <div class="form-group">
    <label for="indoorInput">Indoor o Outdoor</label>
    <select  name="indoor"  type="text" class="form-control" id="indoor">
     <option value = "0">Exterior</option>
     <option value = "1">Interior</option>
    </select>
  </div>
                <input type="hidden" name="createform" value="">

  <input type="hidden" name="createForm" value="create">
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>


</div>
</div>




    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>


</body>
</html>
