<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>


<%-- <div class="container">        
                 <img class="imgcentro" src="/padelunion/img/imgma/tipoPista/pexterior.jpg">           
                <img class="imgcentro" src="/padelunion/img/imgma/tipoPista/ppa.jpg">      
                <img class="imgcentro" src="/padelunion/img/imgma/tipoPista/ppadel.jpg">   
                <img class="imgcentro" src="/padelunion/img/imgma/tipoPista/ppadelotro.jpg">     
</div> --%>

<div class="container">

    <div class="row align-items-center">

            <div class="col-md-4"></div>            

            <div class="col-md-4 text-center">            
                <legend class="headerUserPistas">Pistas de</legend>
                <select name="centro" type="text" class="form-control filtrarPistas" id="centroFiltro">
                    <option selected value="0">-</option>
                    <% for (Centro c : CentroController.getAll()) { %>
                    <option value="<%= c.getId()%>"><%= c.getNombre()%></option>
                    <%}%>                 
                </select>
            </div>

            <div class="col-md-4"></div>            

    </div>            

</div>

<div class="separador50"></div>

<div class="container">
                    
            <div class="table-responsive">
                <table class="table table-striped table-danger table-bordered text-center">

                    <thead>
                        <tr>
                            <th scope="col">Nombre de la pista</th>
                            <th scope="col">Tipo de superficie</th>
                            <th scope="col">Indoor o Outdoor</th>
                            <th scope="col">Comentarios</th>
                            <th scope="col">Añadir comentario</th> 
                        </tr>
                    </thead>

                    <tbody id="marcaPistas">

                    </tbody>

                </table>
            </div>   
                                                                 
                                          
            
</div>
    

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script> 

</body>
</html>