<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>

<% int edad = Integer.parseInt(request.getParameter("edad"));
   int sexo = Integer.parseInt(request.getParameter("sexo"));
   int experiencia = Integer.parseInt(request.getParameter("experiencia"));
   int experienciatenis = Integer.parseInt(request.getParameter("experiencia_tenis"));
   int entreno = Integer.parseInt(request.getParameter("entreno"));
   int fisico = Integer.parseInt(request.getParameter("fisica"));
   int partidos = Integer.parseInt(request.getParameter("partidos"));
   int competicion = Integer.parseInt(request.getParameter("competicion"));
   int drive = Integer.parseInt(request.getParameter("drive"));
   int reves = Integer.parseInt(request.getParameter("reves"));
   int saque = Integer.parseInt(request.getParameter("saque"));
   int resto = Integer.parseInt(request.getParameter("resto"));
   int volea = Integer.parseInt(request.getParameter("volea"));
   int rebotes = Integer.parseInt(request.getParameter("rebotes"));
   int globos = Integer.parseInt(request.getParameter("globos"));
   
  int[] a = {edad,sexo,experiencia,experienciatenis,entreno,fisico,partidos,competicion,
  drive,reves,saque,resto,volea,rebotes,globos};
  int nivel = CalculaNivel.calcularNivel(a);
 
%>                                                                  

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosdavid.css">
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    
</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="container">

<div class="row">
<div class="col">
<h1>Su Nivel es:  <%= CalculaNivel.nivel(CalculaNivel.nivelNumero(nivel))%></h1>
<p>Valor numérico <%= CalculaNivel.nivelNumero(nivel)%></p>
<span></span>
</div>

	
</div>


<div class="row">
<div class="col">
<script src="../js/Chart.js"></script>
<progress id="barrapro" max="100" value="<%= CalculaNivel.calcularNivel(a)%>"></progress>
			<span><%= CalculaNivel.calcularNivel(a)%> %</span>
<div id="canvas-holder">
<canvas id="chart-area" width="200" height="200"></canvas>
<canvas id="chart-area2" width="10" height="10"></canvas>
<canvas id="chart-area3" width="10" height="10"></canvas>
<canvas id="chart-area4" width="10" height="10"></canvas>
		
<script >
var pieData = [{
		value: <%= CalculaNivel.calculaFisico(7,7,7) %>,
		color: "#0b82e7",
		highlight: "#0c62ab",
		label: "Experiencia"
	},
	{
		value: <%= CalculaNivel.calculaFisico(7,7,7) %>,
		color: "#e3e860",
		highlight: "#a9ad47",
		label: "Físico"
	},
	{
		value: 13,
		color: "white",
		highlight: "#b74865",
		label: ""
	},
	{
		value: 35,
		color: "#eb5d82",
		highlight: "#b74865",
		label: "Entrenamiento"
	},
	{
		value: 10,
		color: "#5ae85a",
		highlight: "#42a642",
		label: "Técnica"
	},
];


var ctx = document.getElementById("chart-area").getContext("2d");
var ctx2 = document.getElementById("chart-area2").getContext("2d");
var ctx3 = document.getElementById("chart-area3").getContext("2d");
var ctx4 = document.getElementById("chart-area4").getContext("2d");
window.myPie = new Chart(ctx).Pie(pieData);
</script>
    

		
<script type="text/javascript"> 

  var nivelprogress = CalculaNivel.calcularNivel(a); 
	window.onload = function() { 
		
		animateprogress("#barrapro",nivelprogress);
		
	} 
</script>
</div>

<script src="../js/animateprogress.js"></script>


<table class="table" id="tabla_listado">
  <thead>
    <tr>

      <th scope="col"><%= edad%></th>
      <th scope="col"><%= sexo%></th>
      <th scope="col"><%= experiencia%></th>
      <th scope="col"><%= experienciatenis%></th>
      <th scope="col"><%= entreno%></th>
      <th scope="col"><%= fisico%></th>
      <th scope="col"><%= partidos%></th>
      <th scope="col"><%= competicion%></th>
      <th scope="col"><%= drive%></th>
      <th scope="col"><%= reves%></th>
      <th scope="col"><%= saque%></th>
      <th scope="col"><%= resto%></th>
      <th scope="col"><%= volea%></th>
      <th scope="col"><%= rebotes%></th>
      <th scope="col"><%= globos%></th>
      
    </tr>
  </thead>
  <tbody>

  </tbody>
</table>
<a href="/padelunion/calculanivel/funcionamiento.jsp" class="btn btn-sm btn-primary">Ver funcionamiento</a>
<p></p>
<a href="/padelunion/calculanivel/edit.jsp" class="btn btn-sm btn-primary">Calcula mi nivel</a>
</div>
</div>


</div>

<%-- <link rel="stylesheet" type="text/css" href="./css/estilosdavid.css"> --%>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>

</body>
 
</html>
