<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.sjava.padelunion.*" %>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosdavid.css">
</head>
<body>
<center>
<!-- carrusel de imagenes -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100" src="../img/padel1.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
       <img class="d-block w-100" src="../img/padel2.jpg" alt="First slide">
    </div>
    <div class="carousel-item">
       <img class="d-block w-100" src="../img/padel3.jpg" alt="First slide">

    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

</center>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>


<div class="container">

<div class="text-justify alert alert-danger">
<h2 class="text-center">Valoración de tu nivel de pádel</h2>
<p>Esta herramienta permite conocer <strong>tu nivel de pádel.</strong> </p>

<p>El sistema consiste en valorar el diferente grado de progresión en el control de diferentes tipos de golpe
 para ponderar de una forma lo más objetiva posible cuál es tu nivel de juego.</p>

<p>Una de las grandes dudas que tenemos cuando empezamos a jugar a pádel y vamos mejorando nuestra técnica es conocer nuestro nivel real de juego,
 ya que, a la hora de jugar torneos y partidos con jugadores desconocidos, en general, garantiza el equilibro de los partidos y competiciones,
 llegando a ser más divertidos y efectivos.</p>

<p>Las diferencias de criterio entre diferentes centros así como la percepción subjetiva que tenemos de nuestro nivel de juego de pádel,
 genera situaciones en que acabamos jugando con jugadores con niveles superiores al nuestro o al revés, 
 jugando con personas que tiene un nivel muy inferior al que dicen tener.</p>
</div>

<div class="text-justify alert alert-info">
<h3 class="text-center">Sistemas de valoración</h3>
<p>Básicamente se utilizan tres sistemas de valoración dependiendo de los clubs o zonas en la que practiques pádel:</p>
<ul>
  <li><strong>El sistema de nivel de pádel por letras:</strong> D,  C, C+, B, B+, A, (A). Donde <strong>D</strong> corresponde a iniciación y<strong> A</strong> a profesional (máximo).</li>
  <li><strong>Sistema numérico</strong>: de nivel <strong>1</strong> hasta nivel <strong>7</strong>. 1 corresponde a iniciación y 7 a profesional.</li>
  <li><strong>Sistema de Categorias:</strong>
  <ul>
    <li>Iniciación.</li>
    <li>Intermedio.</li>
    <li>Intermedio alto.</li>
    <li>Avanzado.</li>
    <li>Competición.</li>
    <li>Profesional.</li>
    <li>Profesional máximo.</li>
    </ul>
  </li>
</ul>
<p>Normalmente cuando empezamos a jugar a pádel nuestro nivel es el de iniciación, 
aunque existen factores previos que pueden modificar nuestro nivel de inicio, 
como por ejemplo una experiencia previa en tenis, nuestra facilidad de aprendizaje, etc.</p>
</div>

<div class="text-justify alert alert-info">
<h3 class="text-center">Funcionamiento</h3>
<p>En función de la experiencia, entrenamiento, partidos jugados, forma física, reflejos, 
experiencia en torneos y competiciones, nuestra progresión puede ser más rápida o 
más lenta y, por lo tanto, nuestro nivel de juego puede evolucionar de una forma u
 otra con respecto a otros jugadores.</p>

<p>Utilizamos un cuestionario para que sea el propio jugador 
el que se autocalifique en diferentes facetas de la técnica del pádel y su estilo de juego.</p>

<p>Cada pregunta dispone de varias opciones que equivalen a un grado de progreso del jugador.
 Una vez rellenado y enviado el formulario, la herramienta calcula mediante un algoritmo,  
 unos valores númericos que finalmente tienen una traducción en un nivel de pádel equivalente.
 Este algoritmo puede ser modificado, siempre que mejore la calidad del mismo con alguna futura propuestas.</p>
</div>


<div class="text-justify alert alert-warning" role="alert">
<p>Lógicamente para que el resultado sea fiable y preciso es necesario que el jugador/a 
sea objetivo en sus respuestas y seleccione realmente aquél escenario que mejor define su situación 
real de juego en la pista.</p>
</div>

<div class="text-center">
<a href="/padelunion/calculanivel/edit.jsp" class="btn btn-sm btn-primary">Calcula tu nivel</a>
</div>

<div class="separador50"></div>

</div>
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>


</body>
</html>
