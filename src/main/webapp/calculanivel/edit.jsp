<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.sjava.padelunion.*" %>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosdavid.css">
</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="container">

<form action="finallist.jsp" method="POST">

<fieldset>

    <legend class="text-center headerUserEdit">Calcula tu nivel</legend>

        <div class="form-row">

            <div class="form-group col-md-3"></div>

            <div class="alert alert-warning form-group col-md-6" role="alert">Intenta ser lo más <strong>objetivo</strong> posible para obtener un resultado fiable y preciso</div>

            <div class="form-group col-md-3"></div>

        </div>

        <div class="form-row">

            <div class="form-group col-md-3"></div>

            <div class="form-group col-md-3">
                <label for="Rango de edad"><b>Edad</b></label>
                <select name="edad"  class="form-control">
                    <option value="1" >Hasta 12 años. </option>
                    <option value="2" >Entre 13 y 16 años. </option>
                    <option value="3" >Entre 17 y 20 años. </option>
                    <option value="4" >Entre 21 y 25 años. </option>
                    <option value="5" >Entre 26 y 30 años. </option>
                    <option value="6" >Entre 31 y 40 años. </option>
                    <option value="7" >Entre 41 y 50 años. </option>
                    <option value="8" >Más de 50 años </option>
                </select>
            </div>

            <div class="form-group col-md-3">
                <label for="Sexo"><b>Sexo</b></label>
                <select name="sexo"  class="form-control">
                    <option value="1" >Hombre </option>
                    <option value="2" >Mujer </option>
                </select>
            </div>

            <div class="form-group col-md-3"></div>

        </div>

        <div class="form-row">

            <div class="form-group col-md-3"></div>

            <div class="form-group col-md-3">
                <label for="Experiencia jugando al padel"><b>Experiencia jugando al padel</b></label>
                <select name="experiencia"  class="form-control">
                    <option value="1" >Menos de 3 meses. </option>
                    <option value="2" >Entre 3 meses y 6 meses. </option>
                    <option value="3" >Entre 6 meses y 1 año. </option>
                    <option value="4" >Entre 1 año y 3 años. </option>
                    <option value="5" >Más de 3 años. </option>
                </select>
            </div>

            <div class="form-group col-md-3">
                <label for="Experiencia previa en tenis"><b>Experiencia previa en tenis</b></label>
                <select name="experiencia_tenis"  class="form-control">
                    <option value="1" >No. </option>
                    <option value="2" >Entre 1 año y 3 años. </option>
                    <option value="3" >Entre 3 años y 5 años. </option>
                    <option value="4" >Más de 5 años. </option>
                </select>
            </div>

            <div class="form-group col-md-3"></div>
        
        </div>

        <div class="form-row">

            <div class="form-group col-md-3"></div>

            <div class="form-group col-md-3">
                <label for="Entrenamiento "><b>Entrenamiento</b></label>
                <select name="entreno"  class="form-control">
                    <option value="1" >No he recibido clases. </option>
                    <option value="2" >He realizado menos de 10 clases. </option>
                    <option value="3" >He realizado entre 10 y 20 clases. </option>
                    <option value="5" >He realizado entre 20 y 40 clases. </option>
                    <option value="6" >He realizado entre 40 y 100 clases. </option>
                    <option value="7" >He realizado más de 100 clases. </option>
                    <option value="8" >He relizado más de 100 clases y entreno de forma periódica. </option>
                </select>
            </div>

            <div class="form-group col-md-3">
                <label for="Forma física"><b>Forma física</b></label>
                <select name="fisica"  class="form-control">
                    <option value="1" >Muy baja. </option>
                    <option value="2" >Baja. </option>
                    <option value="3" >Media. </option>
                    <option value="4" >Normal. </option>
                    <option value="5" >Alta. </option>
                    <option value="6" >Muy alta. </option>
                </select>
            </div>

            <div class="form-group col-md-3"></div>
        
        </div>

        <div class="form-row">

            <div class="form-group col-md-3"></div>

            <div class="form-group col-md-3">
                <label for="Partidos a la semana"><b>Partidos por semana</b></label>
                <select name="partidos"  class="form-control">
                    <option value="1" >Un partido por semana. </option>
                    <option value="2" >Dos partidos por semana. </option>
                    <option value="3" >Entre 3 y 5 partidos. </option>
                    <option value="4" >Más de 5 partidos. </option>
                </select>
            </div>

            <div class="form-group col-md-3">
                <label for="Competición"><b>Competición</b></label>
                <select name="competicion"  class="form-control">
                    <option value="1" > Solo partidos entre amigos. </option>
                    <option value="2" > Torneos amistosos. </option>
                    <option value="3" > Ligas amateurs. </option>
                    <option value="4" > Competición/Federado. </option>
                </select>
            </div>

            <div class="form-group col-md-3"></div>
        
        </div>

        <div class="form-row">

            <div class="form-group col-md-1"></div>

            <div class="form-group col-md-10">
                <label for="Golpe de derecha"><b>Golpe de derecha</b></label>
                <select name="drive"  class="form-control">
                    <option value="1" >Me limito a intentar devolver la bola. Golpeo principalmente desde debajo de la pelota. </option>
                    <option value="2" >Aunque mi golpe aun no tiene potencia, he mejorado el movimiento y no golpeo desde abajo la pelota. </option>
                    <option value="3" >Tengo más seguridad, busco colocación, golpeo con algo de potencia. </option>
                    <option value="4" >Puedo mantener peloteos largos, golpeo mayor potencia y bastante colocación. </option>
                    <option value="5" >Consigo colocar la bola donde quiero incluso devolviendo algunas bolas difíciles con variedad, golpe plano o cortado. </option>
                    <option value="6" >Cometo pocos errores, tengo gran seguridad en mi golpe. Golpeo con potencia y agresividad usando golpes planos, cortados o liftados. </option>
                    <option value="7" >Fiablilidad y potencia. Control y variedad del golpe. Dejadas, bolas cortas, volas profundas, efectos tanto en gopes ofensivos como defensivos. </option>
                </select>
            </div>

            <div class="form-group col-md-1"></div>

        </div>

        <div class="form-row">

            <div class="form-group col-md-1"></div>

            <div class="form-group col-md-10">
                <label for="Golpe de revés"><b>Golpe de revés</b></label>
                <select name="reves"  class="form-control">
                    <option value="1" >Intento no golpear de revés, busco siempre la derecha y si golpeo intento simplemente pasarla. </option>
                    <option value="2" >Si viene de revés la golpeo aunque no realizo bien el movimento y no controlo el golpe.</option>
                    <option value="3" >Me coloco mejor para golpear de revés, aunque con poca potencia y velocidad. </option>
                    <option value="4" >Puedo mantener peloteos, golpeo con mayor potencia y colocación aunque limitado a un tipo de revés cortado o plano. </option>
                    <option value="5" >Consigo colocar la bola donde quiero incluso devolviendo algunas bolas difíciles con variedad, golpe plano o cortado. </option>
                    <option value="6" >Cometo pocos errores, tengo gran seguridad en mi golpe. Golpeo con potencia y agresividad usando golpes planos y  cortados. </option>
                    <option value="7" >Fiablilidad y potencia. Control y variedad del golpe. Dejadas, bolas cortas, volas profundas, efectos tanto en gopes ofensivos como defensivos. </option>
                </select>
            </div>

            <div class="form-group col-md-1"></div>            

        </div>

        <div class="form-row">

            <div class="form-group col-md-1"></div>            

            <div class="form-group col-md-10">
                <label for="Saque"><b>Saque</b></label>
                <select name="saque"  class="form-control">
                    <option value="1" >Me limito a intentar que la bola entre el la zona de saque. Golpeo desde abajo de la pelota flojo y alto por encima de la red. </option>
                    <option value="2" >Consigo sacar con algo más de potencia aunque sin mucho control. No me siento seguro y cometo bastantes dobles faltas. </option>
                    <option value="3" >Saco con más potencia aunque fallo bastante. Uso un segundo saque flojo para no cometer doble falta. </option>
                    <option value="4" >Tengo más seguridad y busco saque con más velocidad y colocación cometiendo pocas dobles faltas. </option>
                    <option value="5" >Primer saque con pocos errores, le doy potencia y puedo sacar cortado. Casi sin dobles faltas. </option>
                    <option value="6" >Busco alternar saques a la esquina, centro y a la T casi sin errores. Puedo sacar con potencia, colocación, cortado. Seguro con el segundo saque. </option>
                    <option value="7" >Saco siguiendo una estratégia para el punto en juego. Segundo saque con potencia y colocación.  </option>
                    
                </select>
            </div>

            <div class="form-group col-md-1"></div>      
        
        </div>

        <div class="form-row">

            <div class="form-group col-md-1"></div>      

            <div class="form-group col-md-10">
                <label for="Resto"><b>Resto</b></label>
                <select name="resto"  class="form-control">
                    <option value="1" >Me cuesta restar, sobre todo los saques rápidos y los dirigidos al cristal. </option>
                    <option value="2" >Resto bolas sencillas y lentas, aunque los saques más potentes resto con dificultad dejando bolas fáciles. </option>
                    <option value="3" >Consigo restar con seguridad los saques que no son muy complicados. </option>
                    <option value="4" >Resto con bastante seguridad y dirección de la bola casi todos los saques con relativa potencia. </option>
                    <option value="5" >Tengo resto fiable con control y dirección del resto. </option>
                    <option value="6" >Resto con potencia y colocación. Puedo restar profundo, corto o usando un globo. </option>
                    <option value="7" >Frente a saques complicados, resto de forma agresiva, con potencia, control y variedad. </option>
                </select>
            </div>

            <div class="form-group col-md-1"></div>      
        
        </div>

        <div class="form-row">

            <div class="form-group col-md-1"></div>      

            <div class="form-group col-md-10">
                <label for="Volea"><b>Volea</b></label>
                <select name="volea"  class="form-control">
                    <option value="1" >Casi no subo a la red. Intento volear siempre de derechas y no me coloco bien. Fallo enviando la bola a la red o al cristal. </option>
                    <option value="2" >No me siento seguro en la red. Fallo bastante con la volea de revés y prefiero volear el revés usando la misma cara del drive. </option>
                    <option value="3" >Logro volear de derecha y de revés aunque tengo dificultad en volear bolas rápidas y bolas a los pies. </option>
                    <option value="4" >Tengo buena colocación en la red y voleo con seguridad de drive y revés aunque con poca potencia. </option>
                    <option value="5" >Voleo de drive y de revés direccionando la bola aunque no son voleas agresivas. Empiezo a volear bolas bajas y también rápidas sin que den al cristal. </option>
                    <option value="6" >Buen posicionamiento de pies, tiene potencia, control y profundidad en las voleas de derechas. Errores más comunes cuando golpea con potencia. </option>
                    <option value="7" >Puede golpear a la mayoría de las voleas con profundidad y potencia, juega voleas difíciles con profundidad, buscando el punto débil del rival. </option>
                </select>
            </div>

            <div class="form-group col-md-1"></div>      
    
        </div>

        <div class="form-row">

            <div class="form-group col-md-1"></div>      

            <div class="form-group col-md-10">
                <label for="Rebotes"><b>Rebotes</b></label>
                <select name="rebotes"  class="form-control">
                    <option value="1" >No se como leer los rebotes, y no alcanzo a devolver la pelota despues de un rebote, por lo que golpeo antes de que rebote. </option>
                    <option value="2" >Empiezo a intentar golpear en los rebotes en la pared de fondo pero me cuesta encontrar la distancia y posición. </option>
                    <option value="3" >Tengo más seguridad en los rebotes en la pared de fondo con bolas no muy potentes. Me cuesta devolver los rebotes en doble pared. </option>
                    <option value="4" >Controlo los rebotes a una y dos paredes en velocidades medias y empiezo a devolver algunos rebotes potentes. </option>
                    <option value="5" >Tengo seguridad frente a los rebotes. Alcanzo rebotes rápidos y alterno con algunas bajadas de pared. </option>
                    <option value="6" >Domino los rebotes a una y dos paredes con bolas rápidas. Realizo bajadas de pared de derechas y defiendo con globos con seguridad. </option>
                    <option value="7" >Buena bajada de pared de derecha y de revés. Contesto con agresividad ganando puntos repondiendo rebortes potentes. </option>
                </select>
            </div>

            <div class="form-group col-md-1"></div>      

        </div>

        <div class="form-row">

            <div class="form-group col-md-1"></div>      

            <div class="form-group col-md-10">
                <label for="Globos"><b>Globos</b></label>
                <select name="globos"  class="form-control">
                    <option value="1" >Prácticamente no uso los globos de forma voluntaria. </option>
                    <option value="2" >Intento relizar algún globo pero sin tener clara la dirección. Simplemente lazo la bola alta. </option>
                    <option value="3" >Pruebo a realizar globos con intención pero me quedan cortos o largos con frecuencia. </option>
                    <option value="4" >Tengo mayor control de los globos tanto en globos altos como en medios. Intento colocarlos con intención. </option>
                    <option value="5" >Tengo seguridad con mis globos y cometo pocos errores.  </option>
                    <option value="6" >Realizo globos con  colocación, esquinas y la altura adecuada para defender e intentar volver a recuperar la iniciativa. </option>
                    <option value="7" >Dispongo de recursos para realizar globos con seguridad y precisión en diferentes momentos del juego, no sólo defensivos buscando estrategias. </option>
                </select>
            </div>

            <div class="form-group col-md-1"></div>      

        </div>

        <div class="form-row">
   
            <div class="col-md-12 text-center">
            <input class="btn btn-primary btn-lg " type="submit" name="button" id="button" value="Calcular Nivel" />     
            </div>
    
        </div> 

    </fieldset>
</form>
<div class="separador50"></div>
</div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>


</body>
</html>
