<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>

<% 

   int edad = Integer.parseInt(request.getParameter("edad"));
   int sexo = Integer.parseInt(request.getParameter("sexo"));
   int experiencia = Integer.parseInt(request.getParameter("experiencia"));
   int experienciatenis = Integer.parseInt(request.getParameter("experiencia_tenis"));
   int entreno = Integer.parseInt(request.getParameter("entreno"));
   int fisico = Integer.parseInt(request.getParameter("fisica"));
   int partidos = Integer.parseInt(request.getParameter("partidos"));
   int competicion = Integer.parseInt(request.getParameter("competicion"));
   int drive = Integer.parseInt(request.getParameter("drive"));
   int reves = Integer.parseInt(request.getParameter("reves"));
   int saque = Integer.parseInt(request.getParameter("saque"));
   int resto = Integer.parseInt(request.getParameter("resto"));
   int volea = Integer.parseInt(request.getParameter("volea"));
   int rebotes = Integer.parseInt(request.getParameter("rebotes"));
   int globos = Integer.parseInt(request.getParameter("globos"));
   
  int[] a = {edad,sexo,experiencia,experienciatenis,entreno,fisico,partidos,competicion,
  drive,reves,saque,resto,volea,rebotes,globos};
  int nivel = CalculaNivel.calcularNivel(a);
 
%>                                                                  

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
    
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">


</head>
<style>
#myProgress {
  width: 100%;
  background-color: #ddd;
  border: 1px solid black;
}

#myBar {
  width: 10%;
  height: 30px;
  background-color: #4CAF50;
  text-align: center;
  line-height: 30px;
  color: white;
}
</style>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="container">

  <div class="row align-items-center">  

    <div class="col-md-2"></div>

    <div class="col-md-8">
    
        <div class="alert alert-danger text-center" valign="middle">
            <h5>Su <b>nivel de juego</b> es el de <%= CalculaNivel.nivel(CalculaNivel.nivelNumero(nivel))%></h5>
        </div>

    </div>

    <div class="col-md-2"></div>

  </div>

  <div class="separador30"></div>

  <div class="row align-items-top">

      

      <div class="text-justify alert alert-info col-md-5">
        <h3 class="text-center">Sistemas de valoración</h3>
        <div class="separador20"></div>

          <p>Se utilizan tres sistemas de valoración dependiendo de los clubs o zonas en las que practiques pádel:</p>
          <ul>
            <li><strong>El sistema de nivel de padel por letras:</strong> D, C, C+, B, B+, A, (A) . Donde <strong>D</strong> corresponde a iniciación y<strong> A</strong> a profesional (máximo).</li>
            <li><strong>Sistema numérico</strong>: de nivel <strong>1</strong> hasta nivel <strong>7</strong>. 1 corresponde a iniciación y 7 a profesional.</li>
            <li><strong>Sistema de Categorías:</strong>
            <ul>
              <li>Iniciación.</li>
              <li>Intermedio.</li>
              <li>Intermedio alto.</li>
              <li>Avanzado.</li>
              <li>Competición.</li>
              <li>Profesional.</li>
              <li>Profesional máximo.</li>
              </ul>
            </li>
          </ul>  

  <div class="separador30"></div>

          <div class="form-row">
        
              <div class="col-md-6 text-center">
                <a href="/padelunion/calculanivel/funcionamiento.jsp" class="btn btn btn-primary">Ver funcionamiento</a>
              </div>

              <div class="col-md-6 text-center">
                <a href="/padelunion/calculanivel/edit.jsp" class="btn btn btn-primary">Calcula tu nivel</a>      
              </div>
          
          </div>   
          
          
      </div>	

      <div class="col-md-1"></div>
      
      <div class="text-justify alert alert-info col-md-6">
      <h3 class="text-center">Estadística porcentual del nivel</h3>
        <div class="separador20"></div>
      
              <div id="myProgress">
                  <div id="myBar">0%</div>
              </div>          

              <div class="separador30"></div>              

              <div id="canvas-holder">
                  <canvas id="oilChart" ></canvas>
                  <script src="../js/Chart.min.js"></script>
              </div>

              <div class="alert alert-warning text-justify" role="alert"><p>Normalmente cuando empezamos a jugar a padel nuestro nivel es el de iniciación, 
              aunque existen factores previos que pueden modificar nuestro nivel de inicio, 
              como por ejemplo una experiencia previa en tenis, nuestra facilidad de aprendizaje, etc.</p></div>   
      

      </div>

  </div>

  
</div>

        <div class="separador50"></div>


 
<script>

  var elem = document.getElementById("myBar");   
  var width = 0;
  var id = setInterval(frame, 0);
  function frame() {
    if (width >= <%= nivel%>) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
      elem.innerHTML = width * 1  + '%';
    }
  }

</script>






<script>
      var oilCanvas = document.getElementById("oilChart");

Chart.defaults.global.defaultFontFamily = "Lato";
Chart.defaults.global.defaultFontSize = 17;

var oilData = {
    labels: [
        "Experiencia",
        "Físico",
        "Técnica",
        "Entrenamiento",
        "Aprendizaje"
    ],
    datasets: [
        {
            data: [<%= CalculaNivel.calculaExperiencia(experiencia,experienciatenis)+25%>,
            <%= CalculaNivel.calculaFisico(edad,fisico,partidos)+25%>, 
            <%= CalculaNivel.calculaTecnica(drive,reves,resto,saque,volea,rebotes,globos)+25%>, 
            <%= CalculaNivel.calculaEntreno(entreno, competicion, partidos)+25%>,
            <%= 100-nivel%>],
            backgroundColor: [
                "#FEFF94",    
                "#6384FF",
                "#FF6384",
                "#8463FF",
                "#63FF84"
                
            ],
            borderColor: "white",
            borderWidth: 2
        }]
};

var chartOptions = {
  rotation: -Math.PI,
  cutoutPercentage: 30,
  circumference: Math.PI,
  legend: {
    position: 'left'
  },
  animation: {
    animateRotate: false,
    animateScale: true
  }
};

var pieChart = new Chart(oilCanvas, {
  type: 'doughnut',
  data: oilData,
  options: chartOptions
});
      //# sourceURL=pen.js
    </script>
<script >

var pieData = [{
       
		value: <%= CalculaNivel.calculaFisico(7,7,7) %>,
		color: "#0b82e7",
		highlight: "#0c62ab",
		label: "Experiencia"
	},
	{
		value: <%= CalculaNivel.calculaFisico(7,7,7) %>,
		color: "#e3e860",
		highlight: "#a9ad47",
		label: "Físico"
	},
	{
		value: 13,
		color: "white",
		highlight: "#b74865",
		label: ""
	},
	{
		value: 35,
		color: "#eb5d82",
		highlight: "#b74865",
		label: "Entrenamiento"
	},
	{
		value: 10,
		color: "#5ae85a",
		highlight: "#42a642",
		label: "Técnica"
	},
];


var ctx = document.getElementById("chart-area").getContext("2d");
var ctx2 = document.getElementById("chart-area2").getContext("2d");
var ctx3 = document.getElementById("chart-area3").getContext("2d");
var ctx4 = document.getElementById("chart-area4").getContext("2d");

window.myPie = new Chart(ctx).Pie(pieData);


</script>
    

<%-- <link rel="stylesheet" type="text/css" href="./css/estilosdavid.css"> --%>
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>

</body>
 
</html>
