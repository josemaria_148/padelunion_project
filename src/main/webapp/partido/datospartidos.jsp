<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>

<%

        List<Partido> listaPartidos;
          
        // 1) llegue por GET, desde list.jsp: mostraremos getAll
        // 2) llegue por POST, junto con el resto de cambios

        //verificamos si la petición procede de un POST
        if (false && "POST".equalsIgnoreCase(request.getMethod())) {
            // hemos recibido un POST, reccpilamos el resto de datos...
                
            String hora = "01";
            String dia = "0";
            String sitio = "salamanca";
            int estado = 1;
                               
                //creamos nuevo objeto alumno, que reemplazará al actual del mismo id
       
                listaPartidos = PartidoController.getFiltrar(hora,dia,sitio,estado);
                //redirigimos navegador a la página list.jsp
                response.sendRedirect("/padelunion/partido/datospartidos.jsp");
        } else {
            // hemos recibido un GET, hacemos getAll()
            
                listaPartidos = PartidoController.getAll();
            
        }
		
    

%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosdavid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link href="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/theme-default.min.css" rel="stylesheet" type="text/css" />


</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="separador50"></div>
<center>
<div class="container">
             <form action="#" method="POST">
            <div class="row align-items-center">
                <div class="col-12 col-md-3">
                  
                        <label>Dia</label>
                        <input class="form-control" type="date"  id="horaPartidos" value="2018-06-06">
                 
                </div>
                <div class="col-12 col-md-3">
                        <label>Hora</label>
                        <select  name="jugador2" type="time" class="form-control" id="HoraPartidos">
      
                             <option selected value="9:00">9:00</option>
                            <option value="10:00">10:00</option>      
                            <option value="11:00">11:00</option>
                             <option value="12:00">12:00</option>
         
                        </select>
                </div>
                <div class="col-12 col-md-3">
                          <label>Localidad</label>
                       <select  name="localidad" type="text" class="form-control" id="localidadPartidos">
                               
                                <option selected value="Salamanca">Salamancaa</option>
                                <option value="Barcelona">Barcelona</option>                 
                                <option value="Madrid">Madrid</option>
                                <option  value="SantaMarta">Santa Marta</option>
                                
                        </select>
                </div>
                 <div class="col-12 col-md-3">
                     
                           <button type="submit" class="btn btn-primary" onclick="location.reload()" >Filtrar</button>
 
                </div>
            </div>                     
            </form> 
</div>
         
</center>


<center>
    <div class="container">
                <div class="row align-items-center">    <p></p>  </div>       
               <div class="row align-items-center">   <p></p> </div>                   
    </div>        
</center>



<div class="separador50"></div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/modalRegistro.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script src="/padelunion/js/scripts.js"></script>

    <script>
    $.validate({
        
        modules : 'security'

    });
    </script>
    
    <script>

    $(".btn1").click(function(){

        if($('.btn1').css('background-color', '#555')){
            $(this).css('background-color', 'red');
            $('.btn2').css('background-color', '#555');
            $('.btn3').css('background-color', '#555');
            $('.btn4').css('background-color', '#555');
        } else {
            $(this).css('background-color', '#555');
        } 

    });

    $(".btn2").click(function(){

        if($('.btn2').css('background-color', '#555')){
            $(this).css('background-color', 'red');
            $('.btn1').css('background-color', '#555');
            $('.btn3').css('background-color', '#555');
            $('.btn4').css('background-color', '#555');
        } else {
            $(this).css('background-color', '#555');
        } 

    });;

    $(".btn3").click(function(){

        if($('.btn3').css('background-color', '#555')){
            $(this).css('background-color', 'red');
            $('.btn1').css('background-color', '#555');
            $('.btn2').css('background-color', '#555');
            $('.btn4').css('background-color', '#555');
        } else {
            $(this).css('background-color', '#555');
        } 

    });

    $(".btn4").click(function(){

        if($('.btn4').css('background-color', '#555')){
            $(this).css('background-color', 'red');
            $('.btn1').css('background-color', '#555');
            $('.btn2').css('background-color', '#555');
            $('.btn3').css('background-color', '#555');
        } else {
            $(this).css('background-color', '#555');
        } 

    });

    </script>

</body>
</html>