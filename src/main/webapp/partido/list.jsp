<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>

<%

  //inicializamos las variables loginId y loginName, por el momento sin valor

  int loginId2=0;
  String loginName2=null;

  if (session.getAttribute("loginId") != null) {
        
          loginId2 = (Integer) session.getAttribute("loginId");
          if (loginId2>0){
            loginName2 = (String) session.getAttribute("loginName");
          }

      }
    int admin2 = UsuarioController.EresAdmin(loginId2);

%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosdavid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="container">

<div class="row">
<div class="col">
<h1>Listado Partidos</h1>
</div>
</div>


<div class="row">
<div class="col">


<table class="table" id="tabla_listado">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Hora</th>
      <th scope="col">Día</th>
      <th scope="col">Sitio</th>
     <th scope="col">Estado</th>
     <th scope="col">Resultado</th>
     <th scope="col">Jugador 1</th>
     <th scope="col">Jugador 2</th>
     <th scope="col">Jugador 3</th>
     <th scope="col">Jugador 4</th>
    </tr>
  </thead>
  <tbody>
   <% for (Partido c : PartidoController.getAll()) { %>
        <tr>
        <th scope="row"><%= c.getId() %></th>
        <td><%= c.getHora() %></td>
        <td><%= c.getDia() %></td>
        <td><%= c.getSitio() %></td>
        <td><%= c.getEstado() %></td>
        <td><%= c.getResultado() %></td>
        <td><%= c.getJugador1() %></td>
        <td><%= c.getJugador2() %></td>
        <td><%= c.getJugador3() %></td>
        <td><%= c.getJugador4() %></td>
        <%if(admin2==1){ %>
        <td><a href="/padelunion/partido/edit.jsp?id=<%= c.getId() %>"><i class="fas fa-pencil-alt"></i>
</a></td>
        <td><a href="/padelunion/partido/remove.jsp?id=<%= c.getId() %>"><i class="fas fa-trash-alt"></i>
</a></td>
          <%} %>
        </tr>
    <% } %>
  </tbody>
</table>

<a href="/padelunion/partido/create.jsp" class="btn btn-sm btn-primary">Nuevo partido</a>

</div>
</div>


</div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>
  

</body>
</html>
