<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.sjava.padelunion.*" %>

<%

    Partido p = null;
            
    //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
    request.setCharacterEncoding("UTF-8");

    String id = request.getParameter("id");
    if (id==null){
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/padelunion");
    }else{
        // recibimos id, puede ser que
        // 1) llegue por GET, desde list.jsp: mostraremos los datos para que puedan ser editados
        // 2) llegue por POST, junto con el resto de datos, para guardar los cambios
       
        //verificamos si la petición procede de un POST
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            // hemos recibido un POST, reccpilamos el resto de datos...
                
            int idpartidos = Integer.parseInt(id);
            String hora = request.getParameter("hora");
            String dia = request.getParameter("dia");
            String sitio = request.getParameter("sitio");
            int estado = Integer.parseInt(request.getParameter("estado")); 
            String resultado = "00";
            int idcreador = Integer.parseInt(request.getParameter("idcreador")); 
            int jugador1 = Integer.parseInt(request.getParameter("jugador1"));
            int jugador2 = Integer.parseInt(request.getParameter("jugador2"));
            int jugador3 = Integer.parseInt(request.getParameter("jugador3"));
            int jugador4 = Integer.parseInt(request.getParameter("jugador4"));
            int nivel =  Integer.parseInt(request.getParameter("nivel"));     
                //creamos nuevo objeto alumno, que reemplazará al actual del mismo id
                p = new Partido(idpartidos, hora, MiLib.stringToDate(dia), sitio, estado, resultado, idcreador,
         jugador1, jugador2, jugador3, jugador4,nivel);
                PartidoController.guarda(p);
                //redirigimos navegador a la página list.jsp
                response.sendRedirect("/padelunion/partido/list.jsp");
        } else {
            // hemos recibido un GET, solo tenemos un id
            // pedimos los datos del partido para mostrarlos en el formulario de edición
            p = PartidoController.getId(Integer.parseInt(id));
            if (p==null){
                //no recibimos id, debe ser un error... volvemos a index
             response.sendRedirect("/padelunion");
             }
        }
		
    }

%>
<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Padel Union App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="container">

<div class="row">
<div class="col">
<h1>Editar partido</h1>
</div>
</div>


<div class="row">
<div class="col-md-8">

<form action="#" method="POST">

  <div class="form-group">
    <label for="diaInput">Dia del partido</label>
    <input  name="dia"  type="date" class="form-control" id="diaInput" value="<%= p.getDia() %>" >
  </div>

 <div class="form-group">
    <label for="horaInput">Hora del partido</label>
    <input  name="hora" type="time" min ="8:00:00" max="23:30:00" step="60" class="form-control" id="horaInput" value="<%= p.getHora() %>">
  </div>

   <div class="form-group">
    <label for="sitioInput">Lugar del partido</label>
    <input  name="sitio"  type="text" class="form-control" id="sitioInput" value="<%= p.getHora() %>">
  </div>

  <div class="form-group">
    <label  for="nivelInput">Nivel</label>
    <select  name="nivel" type="number" class="form-control" id="nivelInput">
      
        <option value="1">Iniciaciación</option>
        <option selected value="2">Intermedio</option>      
        <option value="3">Intermedio alto</option>
        <option value="4">Avanzado</option>
        <option value="5">Competición</option>
        <option value="6">Profesional</option>      
        <option value="7">Profesional máximo</option>
                
    </select>
</div>


   <div class="form-group">
    <label for="jugador1Input">Reservar jugador</label>
     <select  name="jugador1" type="checkbox" class="form-control" id="idjugador1Input">
        <% if(p.getJugador1()==0){%>
          <option selected value="0">Disponible</option>
          <option value="<%= p.getJugador1()%>">Ocupado</option>
        <%} else  {%>
         <option value="0">Disponible</option>
          <option selected value="<%= p.getJugador1()%>">Ocupado</option>
          <%}%>
   </select>
   </div>

    <div class="form-group">
    <label for="jugador2Input">Reservar jugador</label>
    <select  name="jugador2" type="checkbox" class="form-control" id="idjugador2Input">
        <% if(p.getJugador2()==0){%>
          <option selected value="0">Disponible</option>
          <option value="<%= p.getJugador2()%>">Ocupado</option>
        <%} else  {%>
         <option value="0">Disponible</option>
          <option selected value="<%= p.getJugador2()%>">Ocupado</option>
          <%}%>
   </select>
   </div>

    <div class="form-group">
    <label for="jugador3Input">Reservar jugador</label>
    <select  name="jugador3" type="checkbox" class="form-control" id="idjugador3Input">
        <% if(p.getJugador3()==0){%>
          <option selected value="0">Disponible</option>
          <option value="<%= p.getJugador3()%>">Ocupado</option>
        <%} else  {%>
         <option value="0">Disponible</option>
          <option selected value="<%= p.getJugador3()%>">Ocupado</option>
          <%}%>
   </select>
   </div>

    <div class="form-group">
     <label for="jugador4Input">Reservar jugador</label>
     
    <select  name="jugador4" type="checkbox" class="form-control" id="idjugador4Input">
        <% if(p.getJugador4()==0){%>
          <option selected value="0">Disponible</option>
          <option value="<%= p.getJugador4()%>">Ocupado</option>
        <%} else  {%>
         <option value="0">Disponible</option>
          <option selected value="<%= p.getJugador4()%>">Ocupado</option>
          <%}%>
   </select>
   </div>
  
  
<!-- guardamos id en campo oculto! -->
    <input type="hidden" name="estado" value="<%= p.getEstado() %>">
    <input type="hidden" name="idcreador" value="<%= p.getIdCreador() %>">
    <input type="hidden" name="id" value="<%= p.getId() %>">
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>




</div>
</div>

</div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>


</body>
</html>
