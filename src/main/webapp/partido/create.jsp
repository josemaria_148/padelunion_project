<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>
<%@page import="java.util.Date" %>

<%
    boolean datosOk;

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo partido
    
            //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
        request.setCharacterEncoding("UTF-8");


        String hora = request.getParameter("hora");
        String dia =request.getParameter("dia");
        String sitio = request.getParameter("sitio");
        int estado=0;
        String resultado = "00";
        int idcreador = Integer.parseInt(request.getParameter("idcreador"));;//(idusuario logado)
        int jugador1 = Integer.parseInt(request.getParameter("jugador1"));
        int jugador2 = Integer.parseInt(request.getParameter("jugador2"));
        int jugador3 = Integer.parseInt(request.getParameter("jugador3"));
        int jugador4 = Integer.parseInt(request.getParameter("jugador4"));
        int nivel = Integer.parseInt(request.getParameter("nivel"));

        // aquí verificaríamos que todo ok, y solo si todo ok hacemos guarda, 
        // por el momento lo damos por bueno...
        datosOk=true;
        if (datosOk){
            Partido p = new Partido(hora,  MiLib.stringToDate(dia), sitio, estado, resultado, idcreador,jugador1,jugador2,jugador3,jugador4,nivel);
            PartidoController.guarda(p);
            // nos vamos a mostrar la lista, que ya contendrá el nuevo partido
            response.sendRedirect("/padelunion/partido/mispartidos.jsp");
            
        
         }else {  
                response.sendRedirect("/padelunion/index.jsp");
              
                }
    }

%>



<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Padel Union App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">    
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
</head>
<body>

<%@include file="/parts/login.jsp"%>

<%@include file="/parts/menu.jsp"%>




<div class="container">

<%-- <div class="row">
<div class="col">
<h1>Nuevo partido</h1>
</div>
</div> --%>


<form class="form-horizontal" action="#" method="POST">
    <fieldset>
        <legend class="text-center headerUserEdit">Nuevo partido</legend>

        <div class="form-row">

            <div class="form-group col-md-3"></div>

            <div class="form-group col-md-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
                    </div>
                    <input  name="dia"  type="date" class="form-control" id="diaInput" >
                </div>
            </div>

            <div class="form-group col-md-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-clock"></i></div>
                    </div>
                    <input  name="hora"  type="time" class="form-control" id="horaInput">
                </div>
            </div>

            <div class="form-group col-md-3"></div>

        </div>

        <div class="form-row">

            <div class="form-group col-md-3"></div>

            <div class="form-group col-md-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-map-marker-alt"></i></div>
                    </div>
                    <input  name="sitio"  type="text" class="form-control" id="sitioInput" placeholder="Localidad">
                </div>
            </div>

            <div class="form-group col-md-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-signal"></i></div>
                    </div>
                    <select  name="nivel" type="number" class="form-control" id="nivelInput">
                    
                        <option value="1">Iniciaciación</option>
                        <option selected value="2">Intermedio</option>      
                        <option value="3">Intermedio alto</option>
                        <option value="4">Avanzado</option>
                        <option value="5">Competición</option>
                        <option value="6">Profesional</option>      
                        <option value="7">Profesional máximo</option>
                                
                    </select>
                </div>
            </div>

            <div class="form-group col-md-3"></div>

        </div>

        <div class="form-row">

            <div class="form-group col-md-3"></div>

            <div class="form-group col-md-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-user"></i></div>
                    </div>
                    <select  name="jugador1" type="text" class="form-control" id="jugador1Input">
                        <option value="0">Disponible</option>
                        <option value="<%= loginId%>">Ocupado</option>
                    </select>
                </div>
            </div>

            <div class="form-group col-md-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-user"></i></div>
                    </div>
                    <select  name="jugador2" type="text" class="form-control" id="jugador2Input">
                        <option value="0">Disponible</option>
                        <option value="<%= loginId%>">Ocupado</option>
                    </select>
                </div>
            </div>

            <div class="form-group col-md-3"></div>

        </div>

        <div class="form-row">

            <div class="form-group col-md-3"></div>

            <div class="form-group col-md-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-user"></i></div>
                    </div>
                    <select  name="jugador3" type="text" class="form-control" id="jugador3Input">
                        <option value="0">Disponible</option>
                        <option value="<%= loginId%>">Ocupado</option>
                    </select>
                </div>
            </div>

            <div class="form-group col-md-3">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text"><i class="fas fa-user"></i></div>
                    </div>
                    <select  name="jugador4" type="text" class="form-control" id="jugador4Input">
                        <option value="0">Disponible</option>
                        <option value="<%= loginId%>">Ocupado</option>
                    </select>
                </div>
            </div>

            <div class="form-group col-md-3"></div>

        </div>
            <input type="hidden" name="idcreador" value="<%= loginId%>">
        <div class="form-row">
   
            <div class="col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Guardar</button>      
            </div>
    
        </div>    
    </fieldset>
</form>

<%--  Este div container --%>
</div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>


</body>
</html>
