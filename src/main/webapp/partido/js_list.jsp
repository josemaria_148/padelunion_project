<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnionApp</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>


<%-- <style>
img.foto100 {
  width: 150px;
}
</style> --%>

<div class="container">




<div class="row">
<div class="col">
<button id="cargajson" class="btn btn-sm btn-danger">Carga JSON</button>
<button onclick="pintaDatos(1)" class="btn btn-sm btn-success">Avatar1</button>
<button onclick="pintaDatos(2)" class="btn btn-sm btn-success">Avatar2</button>
<br><br>

<table class="table" id="tabla_listado">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col"></th>
      <th scope="col">Nombre</th>
      <th scope="col">Email</th>
      <th scope="col">Teléfono</th>
      <th scope="col">Fecha</th>
      <% if (loginId>0) { %>
        <th scope="col"></th>
        <th scope="col"></th>
        <th scope="col"></th>
      <% } %>
    </tr>
  </thead>
  <tbody id="bodyTabla">

  </tbody>
</table>



</div>
</div>


</div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>

<script>

var listaUsuarios;

$(document).on("click", "#cargajson", function(){

    $.getJSON("/padelunion/usuario/listjson.jsp", function (data) {
        listaUsuarios=data;
        pintaDatos(0);
    });

});


function pintaDatos(avatar){
    
    $('#bodyTabla').html("");

    listaUsuarios.forEach(function (usuario) {

        var mostrarUsuario=true;
        
        if (avatar>0) {
            if (usuario.urlfoto.indexOf("avatar"+avatar) === -1) {
                mostrarUsuario=false;
                console.log("no:"+usuario.urlfoto);
            }
        }

        if (mostrarUsuario==true){

            var fila = $('<tr>');
            fila.append($('<td>').html(alumno.id));
            fila.append($('<td>').html('<img class="img-thumbnail foto100" src="/academiapp/uploads/'+alumno.urlfoto+'" />'));
            fila.append($('<td>').html(alumno.nombre));
            fila.append($('<td>').html(alumno.email));
            fila.append($('<td>').html(alumno.telefono));
            fila.append($('<td>').html(alumno.alta));
        
            $('#bodyTabla').append(fila);
        }


        });
}
     



</script>

</body>
</html>
