<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>
<body>

<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>


<div class="container">
            
  <div class="row align-items-center">

      <div class="col-md-4"></div>            

        <div class="col-md-4 text-center">  

          <legend class="headerUserComentarios">Listado de centros</legend>

        </div>

      <div class="col-md-4"></div>            
      
    </div>

  <div class="separador50"></div>

    <% for (Centro c : CentroController.getAll()) { %>
        
      <div class="row align-items-center">  

          <div class="col-md-3">
            <table class="table table-striped table-info table-bordered text-center" id="tabla_listadocentrosinfo">
                <tbody>
                  <img class="imgcentro" src="/padelunion/img/imgma/<%= c.getFoto() %>">
                 </tbody>
              </table> 
          </div> 

          <div class="col-md-9">
            
              <table class="table table-striped table-success table-bordered text-center" id="tabla_listadocentrosinfo">
                <tbody>
              
                      <tr>
                      <th>Nombre</th> <td><%= c.getNombre()%></td>  <th>Dirección</th> <td><%= c.getUbicacion()%></td>
                      </tr>
                      <tr>
                      <th>Teléfono</th> <td><%= c.getTelefono()%></td> <th>Email</th> <td><%= c.getEmail()%></td>
                      </tr>
                      <tr>
                      <th>Url</th> <td><%= c.getUrl()%></td> <th>Número de pistas</th> <td><%= PistaController.numeroPistas(c.getId())%></td>
                      </tr>
                  
                </tbody>
              </table>      
          </div>
        </div>
        <div class="separador50"></div>
      <%}%>
   

</div>
    
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script> 

</body>
</html>
