CREATE DATABASE  IF NOT EXISTS `padel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `padel`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: padel
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `centros`
--

DROP TABLE IF EXISTS `centros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centros` (
  `idcentros` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL COMMENT 'Nombre de la pista. Lugar generico. ',
  `ubicacion` varchar(100) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `localidad` varchar(100) DEFAULT NULL,
  `foto` varchar(200) DEFAULT 'nuevocentro.jpg',
  PRIMARY KEY (`idcentros`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centros`
--

LOCK TABLES `centros` WRITE;
/*!40000 ALTER TABLE `centros` DISABLE KEYS */;
INSERT INTO `centros` VALUES (1,'Padel Planet Salamanca','Calle Juan de la Cierva, 4','665692190','WWW.PADELPLANETSALAMANCA.COM','PADELPLANETSALAMANCA@PADELPLANET.COM','Carbajosa de la Sagrada','padelplanet.jpg'),(2,'Mas que Padel Salamanca ','Polígono El Montalvo III, Calle Primera, 41','633737595','WWW.MASQUEPADEL.COM','MASQUEPADEL@MASQUEPADEL.COM',NULL,NULL),(3,'Club Pádel El Viñal','Urbanización Almacenes, Calle Duero, 8 Sector A1','652333584','WWW.CLUBPADELELVINAL.COM','ELVINAL@ELVINAL.COM','Villares de la Reina','padelelvinal.jpg'),(4,'Padelhome','Calle Zapardiel, 2','923337595','WWW.PADELHOME.COM','PADELHOME@PADELHOME.COM','Villamayor ','padelhome.jpg'),(5,'Padel You','Calle Conde Orgaz,14','923239140','WWW.PADELYOU.COM','PADELYOU@PADELYOU.COM','Salamanca','padelyou.jpg'),(6,'All Padel Academy','Calle Vertical 8a, 17','627626846','WWW.ALLPADELACADEMUY.COM','PADELACADEMY@PADELACADEMY.COM','Carbajosa de la Sagrada','allpadelacademy.jpg'),(7,'Padel Palace - Salamanca','Calz. Toro, 23','923204118','WWW.PADELPALACE.COM','PADELPALACE@PADELPALACE.COM','Villares de la Reina','padelpalace.jpg'),(8,'Club Padel Time','Calle de Francisco Bonet, 4','620422105','WWW.CLUBPADELTIME.COM','CLUBPADELTIME@CLUBPADELTIME.COM','Carbajosa de la Sagrada','clubpadeltime.jpg'),(9,'Pistas Ayum. Cabrerizos','Av. Los Rosales, 33','923289063','WWW.AYUNTAMIENTOCABRERIZOS.COM','PADEL@AYUNTAMIENTOCABRERIZOS.COM','Cabrerizos','clubteniscabrerizos.jpg'),(10,'Pista Ayum. Carrascal','Calle Peñasolana III, 53','923568978','WWW.AYUNTAMIENTOCARRASCAL.COM','PADEL@AYUNTAMIENTOCARRASCAL.COM','Carrascal de Barrega','padelayuncarrascal.jpg'),(11,'Ciudad deportiva de la Aldehuela','Av. de la Aldehuela, S/N','923090518','WWW.CIUDADALDEHUELA.COM','PADEL@ALDEHUELA.COM','Salamanca','padelaldehuela.jpg'),(12,'Club de Tenis Cabrerizos','Calle Villares, 14','923080107','WWW.CLUBDETENISCABRERIZOS.COM','PADEL@CABRERIZOS.COM','Moriscos','clubteniscabrerizos.jpg'),(13,'Salas Bajas','Puente de Sánchez Fabrés, s/n','923294639','WWW.SALASBAJAS.COM','SALASBAJAS@SALABAJAS.COM','Salamanca','salasBajas.jpg'),(14,'Complejo deportivo Vicente del Bosque','Calle Rafael Lapesa','923232785','WWW.CLUBVICENTEDELBOSQUE.COM','PADEL@VICENTEDELBOSQUE','Salamanca','vicentedelbosque.jpg');
/*!40000 ALTER TABLE `centros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comentarios_pista`
--

DROP TABLE IF EXISTS `comentarios_pista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentarios_pista` (
  `idcomentarios` int(11) NOT NULL AUTO_INCREMENT,
  `comentarios` varchar(3000) NOT NULL COMMENT 'Comentarios del usuario. Estado. Tipo. ',
  `puntuacion` int(5) DEFAULT NULL COMMENT 'Puntuacion del 1-10. La da el usuario',
  `idpistas` int(11) DEFAULT NULL,
  `idusuarios` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcomentarios`),
  KEY `fk_Comentarios_pista_Pistas1_idx` (`idpistas`),
  KEY `fk_Comentarios_pista_Usuarios1_idx` (`idusuarios`),
  CONSTRAINT `fk_Comentarios_pista_Pistas1` FOREIGN KEY (`idpistas`) REFERENCES `pistas` (`idpistas`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Comentarios_pista_Usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentarios_pista`
--

LOCK TABLES `comentarios_pista` WRITE;
/*!40000 ALTER TABLE `comentarios_pista` DISABLE KEYS */;
INSERT INTO `comentarios_pista` VALUES (27,'PISTA NUEVA, MUY BUENA',5,1,2),(28,'PISTA EN MAL ESTADO. NO RECOMIENTO',1,1,3),(29,'ESPACIO EN BUEN ESTADO. BUEN AMBIENTE',5,2,3),(31,'PISTA RECOMENDADA',5,3,4),(32,'NO RECOMIENDO, COBRAN POR LA LUZ',1,3,6),(33,'MUY BUENA',5,4,5),(34,'PISTA NUEVA, MUY BUENA',5,5,1),(35,'BUENA LOCALIZACION. RED EN BUEN ESTADO. RECOMENDADA',5,4,2),(36,'PISTA NORMAL',3,5,5),(37,'LA RECOMIENDO, PISTA NUEVA',5,5,6),(39,'FIRME EN BUEN ESTADO. RECOMIENDO',5,1,6),(40,'Podía ser más barata, 5 euros por persona ',0,8,10),(41,'Pista nueva, precio 2 euros persona  ',3,1,10);
/*!40000 ALTER TABLE `comentarios_pista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partidos`
--

DROP TABLE IF EXISTS `partidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partidos` (
  `idpartidos` int(11) NOT NULL AUTO_INCREMENT,
  `hora` varchar(10) DEFAULT NULL,
  `dia` date DEFAULT NULL,
  `sitio` varchar(500) DEFAULT NULL COMMENT 'Si hay una pista determinada o alguna indicacion que de el creador',
  `estado` int(11) DEFAULT NULL COMMENT 'Si el partido esta en proceso 0, en juego 1, finalizado 2 . ',
  `resultado` varchar(10) DEFAULT NULL,
  `idcreador` int(11) DEFAULT NULL COMMENT 'Este es el creador.',
  `idcentros` int(11) DEFAULT NULL,
  `jugador1` int(11) DEFAULT '0',
  `jugador2` int(11) DEFAULT '0',
  `jugador3` int(11) DEFAULT '0',
  `jugador4` int(11) DEFAULT '0',
  `nivel` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpartidos`),
  KEY `fk_Partidos_Centros1_idx` (`idcentros`),
  CONSTRAINT `fk_Partidos_Centros1` FOREIGN KEY (`idcentros`) REFERENCES `centros` (`idcentros`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partidos`
--

LOCK TABLES `partidos` WRITE;
/*!40000 ALTER TABLE `partidos` DISABLE KEYS */;
INSERT INTO `partidos` VALUES (1,'20:00','2018-06-30','Salamanca',0,NULL,5,9,5,4,0,0,4),(2,'12:15','2018-06-29','Salamanca',1,NULL,5,8,5,7,9,0,4),(3,'09:00','2018-07-01','Salamanca',0,NULL,3,9,3,5,0,0,4),(4,'20:00','2018-07-01','Salamanca',0,NULL,4,2,4,0,0,0,4),(5,'21:00','2018-07-10','Salamanca',1,NULL,4,6,4,3,5,0,4),(6,'11:30','2018-07-06','Salamanca',0,NULL,5,5,5,3,0,0,4),(7,'21:00','2018-07-07','Salamanca',0,NULL,5,2,5,9,0,0,4),(8,'22:00','2018-07-02','Salamanca',1,NULL,5,2,5,0,0,0,4),(9,'21:30','2018-07-02','Salamanca',0,NULL,3,2,3,9,0,0,4),(10,'10:01','2018-06-04','SALAMANCA',0,'00',2,NULL,0,2,0,0,2),(11,'','2018-06-12','',0,'00',8,NULL,0,0,8,0,2),(12,'','2018-06-06','',0,'00',10,NULL,0,0,0,0,2);
/*!40000 ALTER TABLE `partidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pistas`
--

DROP TABLE IF EXISTS `pistas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pistas` (
  `idpistas` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL COMMENT 'Nombre de la pista. Puede ser un identificativo como nombre del centro y del numero de pista',
  `tipo` int(2) DEFAULT NULL COMMENT 'Tipo de suelo (cemento 0, moqueta1, otro 2). ',
  `indoor` int(2) DEFAULT NULL COMMENT 'Si es exterior(0) e interior(1). ',
  `idcentros` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpistas`),
  KEY `fk_Pistas_Centros1_idx` (`idcentros`),
  CONSTRAINT `fk_Pistas_Centros1` FOREIGN KEY (`idcentros`) REFERENCES `centros` (`idcentros`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pistas`
--

LOCK TABLES `pistas` WRITE;
/*!40000 ALTER TABLE `pistas` DISABLE KEYS */;
INSERT INTO `pistas` VALUES (1,'PISTA 1',0,0,1),(2,'PISTA 1',0,0,2),(3,'PISTA 1',0,1,3),(4,'PISTA 1',1,0,4),(5,'PISTA 1',1,0,5),(6,'PISTA 1',0,1,6),(7,'PISTA 1',0,0,7),(8,'PISTA 1',2,1,8),(9,'PISTA 1',2,1,9),(10,'PISTA 1',0,0,10);
/*!40000 ALTER TABLE `pistas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `clave` varchar(150) DEFAULT NULL,
  `fechanacimiento` date DEFAULT NULL,
  `fechainscripcion` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha en que se da de alta en la base de datos. Pongo Timestamp para que se rellene automaticamente',
  `nivel` int(8) DEFAULT NULL COMMENT 'Nivel que tiene el usuario. Pongo un entero porque el nivel va de 1-7',
  `mano` int(2) DEFAULT '1' COMMENT 'Si el jugador es diestro o zurdo. Sirve para hacer estadisticas\nDiestro 1 y zurdo 0',
  `sexo` int(2) DEFAULT NULL,
  `administrador` int(4) DEFAULT '0' COMMENT 'Este campo, es para saber si puede ser creador de partidas o no. Los jugadores no son administradores. Solo para modificar partidas internamente y el tablon anuncios',
  `telefono` varchar(45) DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL COMMENT 'Puntos que acomula el usuario por partidos ganados',
  PRIMARY KEY (`idusuarios`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'MARIA ','SALAS','MARIA@MARIA.COM','*89C6B530AA78695E257E55D63C00A6EC9AD3E977','1990-04-02','2018-06-25 16:55:36',6,1,1,0,'99999',NULL),(2,'JUAN','SANCHEZ','JUAN@JUAN.COM','*89C6B530AA78695E257E55D63C00A6EC9AD3E977','2000-10-14','2018-06-25 16:55:36',1,1,0,0,'923589674',NULL),(3,'LUCIA ','LOPEZ A','LUCIA@LUCIA.COM','*89C6B530AA78695E257E55D63C00A6EC9AD3E977','1998-08-11','2018-06-25 16:55:36',4,1,1,0,'778956428',NULL),(4,'FERNANDO','TORNO','FERNANDO@FERNANDO.COM','*89C6B530AA78695E257E55D63C00A6EC9AD3E977','1995-08-08','2018-06-25 16:55:36',3,0,0,0,'949258756',NULL),(5,'MANUEL','DOMINGUEZ','MANUEL@MANUEL.COM','*89C6B530AA78695E257E55D63C00A6EC9AD3E977','1985-09-14','2018-06-25 16:55:36',2,0,0,0,'689541266',NULL),(6,'RICARD','RICARD','RICARD@RICARD.COM','*89C6B530AA78695E257E55D63C00A6EC9AD3E977','2001-01-01','2018-06-25 16:55:36',1,1,0,0,'652987412',NULL),(7,'PEPE','BLAZQUEZ','PEPE@PEPE.COM','*89C6B530AA78695E257E55D63C00A6EC9AD3E977','2000-05-17','2018-06-25 16:55:36',6,1,0,1,'985632148',NULL),(8,'DAVID','GOMEZ','DAVID@DAVID.COM','*89C6B530AA78695E257E55D63C00A6EC9AD3E977','1988-02-09','2018-06-25 16:55:36',3,1,0,1,'637688596',NULL),(9,'ANGELES','SANCHEZ','angeles@angeles.com','*89C6B530AA78695E257E55D63C00A6EC9AD3E977','1982-09-29','2018-06-25 16:55:36',2,1,0,1,'923568978',NULL),(10,'Jose','Garcia','rena693@hotmail.com','*E9D057131C22A0D76B4AAD2C61655BDFA706E637','1997-06-20','2018-06-25 17:32:15',3,1,1,0,'666001001',NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuariospartidos`
--

DROP TABLE IF EXISTS `usuariospartidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuariospartidos` (
  `idcreador` int(11) NOT NULL COMMENT 'Identificador de cada uno de los jugadores que tiene un partido. No tiene nada que ver con el creador del partido',
  `idpartidos` int(11) DEFAULT NULL,
  `jugador1` int(11) DEFAULT NULL,
  `jugador2` int(11) DEFAULT NULL,
  `jugador3` int(11) DEFAULT NULL,
  `jugador4` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcreador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuariospartidos`
--

LOCK TABLES `usuariospartidos` WRITE;
/*!40000 ALTER TABLE `usuariospartidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuariospartidos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-25 18:49:25
