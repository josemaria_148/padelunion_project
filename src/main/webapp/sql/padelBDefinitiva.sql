CREATE DATABASE  IF NOT EXISTS `padel` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `padel`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: padel
-- ------------------------------------------------------
-- Server version	5.6.35

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `centros`
--

DROP TABLE IF EXISTS `centros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `centros` (
  `idcentros` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL COMMENT 'Nombre de la pista. Lugar generico. ',
  `ubicacion` varchar(100) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `localidad` varchar(100) DEFAULT NULL,
  `foto` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`idcentros`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `centros`
--

LOCK TABLES `centros` WRITE;
/*!40000 ALTER TABLE `centros` DISABLE KEYS */;
INSERT INTO `centros` VALUES (1,'Padel Planet Salamanca','Calle Juan de la Cierva, 4','665692190',NULL,NULL,'Carbajosa de la Sagrada',NULL),(2,'Mas que Padel Salamanca','Polígono El Montalvo III, Calle Primera, 41','633737595',NULL,'A@A.COM','Carbajosa de la Sagrada',NULL),(5,'Padelhome','Calle Zapardiel, 2','923337595',NULL,NULL,'Villamayor ',NULL),(8,'Padel Palace - Salamanca ','Calz. Toro, 23','923204118','www.centro.com','null',NULL,NULL),(9,'Club Padel Time','Calle de Francisco Bonet, 4','620422105',NULL,NULL,'Carbajosa de la Sagrada',NULL),(12,'Ciudad deportiva de la Aldehuela ','Av. de la Aldehuela, S/N','923090518','WWW','null',NULL,NULL),(14,'Salas Bajas','Puente de Sánchez Fabrés, s/n','923294639',NULL,NULL,'Salamanca',NULL),(17,'Java','salamanca','6376899889','er','oscar@gamil.com',NULL,NULL);
/*!40000 ALTER TABLE `centros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comentarios_pista`
--

DROP TABLE IF EXISTS `comentarios_pista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comentarios_pista` (
  `idcomentarios` int(11) NOT NULL AUTO_INCREMENT,
  `comentarios` varchar(3000) NOT NULL COMMENT 'Comentarios del usuario. Estado. Tipo. ',
  `puntuacion` int(5) DEFAULT NULL COMMENT 'Puntuacion del 1-10. La da el usuario',
  `idpistas` int(11) DEFAULT NULL,
  `idusuarios` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcomentarios`),
  KEY `fk_Comentarios_pista_Pistas1_idx` (`idpistas`),
  KEY `fk_Comentarios_pista_Usuarios1_idx` (`idusuarios`),
  CONSTRAINT `fk_Comentarios_pista_Pistas1` FOREIGN KEY (`idpistas`) REFERENCES `pistas` (`idpistas`) ON DELETE NO ACTION ON UPDATE SET NULL,
  CONSTRAINT `fk_Comentarios_pista_Usuarios1` FOREIGN KEY (`idusuarios`) REFERENCES `usuarios` (`idusuarios`) ON DELETE SET NULL ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comentarios_pista`
--

LOCK TABLES `comentarios_pista` WRITE;
/*!40000 ALTER TABLE `comentarios_pista` DISABLE KEYS */;
INSERT INTO `comentarios_pista` VALUES (1,'buena, ',3,2,11),(2,'sucia, ',1,3,9),(3,'buena, ',5,1,7),(4,'buena, ',5,4,1),(5,'mala, ',1,1,NULL),(6,'mala, ',1,2,NULL),(7,'no es buena, ',1,2,NULL);
/*!40000 ALTER TABLE `comentarios_pista` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `partidos`
--

DROP TABLE IF EXISTS `partidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `partidos` (
  `idpartidos` int(11) NOT NULL AUTO_INCREMENT,
  `hora` varchar(10) DEFAULT NULL,
  `dia` date DEFAULT NULL,
  `sitio` varchar(500) DEFAULT NULL COMMENT 'Si hay una pista determinada o alguna indicacion que de el creador',
  `estado` int(11) DEFAULT NULL COMMENT 'Si el partido esta en proceso 0, en juego 1, finalizado 2 . ',
  `resultado` varchar(10) DEFAULT NULL,
  `idcreador` int(11) DEFAULT NULL COMMENT 'Este es el creador.',
  `idcentros` int(11) DEFAULT NULL,
  `jugador1` int(11) DEFAULT '0',
  `jugador2` int(11) DEFAULT '0',
  `jugador3` int(11) DEFAULT '0',
  `jugador4` int(11) DEFAULT '0',
  PRIMARY KEY (`idpartidos`),
  KEY `fk_Partidos_Centros1_idx` (`idcentros`),
  CONSTRAINT `fk_Partidos_Centros1` FOREIGN KEY (`idcentros`) REFERENCES `centros` (`idcentros`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `partidos`
--

LOCK TABLES `partidos` WRITE;
/*!40000 ALTER TABLE `partidos` DISABLE KEYS */;
INSERT INTO `partidos` VALUES (1,'01:01','2018-06-15','peñad',0,'00',2,2,0,1,1,0),(4,NULL,'2018-06-20','PISTA 2',NULL,NULL,NULL,1,1,0,1,0),(5,NULL,'2018-06-21','PISTA 1',NULL,NULL,NULL,2,1,0,1,0),(8,NULL,'2018-06-22','PISTA 4',NULL,NULL,NULL,1,1,0,1,0),(9,'','2018-06-15','null',0,'00',2,1,1,0,1,0),(10,'02:01','2018-06-13','SALAMANCA',0,'00',2,NULL,1,0,1,0),(11,'','2018-06-14','SALAMANCA',0,'00',2,NULL,1,0,1,0),(12,'01:05','2018-06-21','asal',0,'00',2,NULL,1,0,1,0),(13,'','2018-06-14','SALAMANCA',0,'00',2,NULL,1,0,1,0),(15,'','2018-06-13','',0,'00',2,NULL,0,0,0,0),(16,'09:09','2018-06-01','',0,'00',2,NULL,0,1,0,0),(17,'00:00','2018-06-12','SALAMANCA',0,'00',2,NULL,0,0,0,0),(18,'05:05','2018-06-07','05:05',0,'00',2,NULL,0,0,0,0),(19,'00:04','2018-06-13','00:04',0,'00',2,NULL,0,0,0,0),(20,'01:01','2018-06-14','SALAMANCA',0,'00',2,NULL,1,1,1,0),(23,'','2018-06-14','',0,'00',2,NULL,NULL,NULL,NULL,NULL),(25,'02:03','2018-07-07','SALAMANCA',0,'00',2,NULL,0,0,0,0),(27,'03:03','2018-06-06','SALAMANCA',0,'00',2,NULL,1,0,0,1),(28,'09:08','2018-08-08','Madrid',0,'0',1,NULL,1,1,1,0),(29,NULL,'2018-04-05',NULL,1,'0',1,NULL,1,1,1,1);
/*!40000 ALTER TABLE `partidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pistas`
--

DROP TABLE IF EXISTS `pistas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pistas` (
  `idpistas` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL COMMENT 'Nombre de la pista. Puede ser un identificativo como nombre del centro y del numero de pista',
  `tipo` int(2) DEFAULT NULL COMMENT 'Tipo de suelo (cemento 0, moqueta1, otro 2). ',
  `indoor` int(2) DEFAULT NULL COMMENT 'Si es exterior(0) e interior(1). ',
  `idcentros` int(11) DEFAULT NULL,
  PRIMARY KEY (`idpistas`),
  KEY `fk_Pistas_Centros1_idx` (`idcentros`),
  CONSTRAINT `fk_Pistas_Centros1` FOREIGN KEY (`idcentros`) REFERENCES `centros` (`idcentros`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pistas`
--

LOCK TABLES `pistas` WRITE;
/*!40000 ALTER TABLE `pistas` DISABLE KEYS */;
INSERT INTO `pistas` VALUES (1,'PISTA1 gf',0,0,1),(2,'PISTA2 ',1,1,1),(3,'PISTA1',1,0,1),(4,'PISTA2 ',1,0,1),(6,'Java',1,1,14),(7,'Java',0,0,1),(8,'sql',2,1,1);
/*!40000 ALTER TABLE `pistas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idusuarios` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `clave` varchar(150) DEFAULT NULL,
  `fechanacimiento` date DEFAULT NULL,
  `fechainscripcion` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Fecha en que se da de alta en la base de datos. Pongo Timestamp para que se rellene automaticamente',
  `nivel` int(8) DEFAULT NULL COMMENT 'Nivel que tiene el usuario. Pongo un entero porque el nivel va de 1-7',
  `mano` int(2) DEFAULT '1' COMMENT 'Si el jugador es diestro o zurdo. Sirve para hacer estadisticas',
  `sexo` int(2) DEFAULT NULL,
  `administrador` int(4) DEFAULT '0' COMMENT 'Este campo, es para saber si puede ser creador de partidas o no. Los jugadores no son administradores. Solo para modificar partidas internamente y el tablon anuncios',
  `telefono` varchar(45) DEFAULT NULL,
  `ranking` int(11) DEFAULT NULL COMMENT 'Puntos que acomula el usuario por partidos ganados',
  PRIMARY KEY (`idusuarios`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'MARIA ','SALAS','A@A.OM','*49A5E44A73FE78E55F44540A0AF1A6A068F347F0','1990-04-02','2018-06-13 11:55:38',6,1,1,0,'99999',NULL),(4,'JUAN','SANCHEZ','J@JJ.COM','*49A5E44A73FE78E55F44540A0AF1A6A068F347F0','2000-10-14','2018-06-13 12:02:19',1,1,0,0,'7788',NULL),(7,'LUCIA ','LOPEZ A','L@AA.COM','*49A5E44A73FE78E55F44540A0AF1A6A068F347F0','1998-08-11','2018-06-13 12:02:19',4,1,1,0,'85554444',NULL),(9,'FERNANDO','TORNO','F@AA.COM','*49A5E44A73FE78E55F44540A0AF1A6A068F347F0','1995-08-08','2018-06-13 12:02:19',3,0,0,0,'9998',NULL),(11,'MANUEL','DOMINGUEZ','M@MM.COM','*49A5E44A73FE78E55F44540A0AF1A6A068F347F0','1985-09-14','2018-06-13 12:02:19',2,0,0,0,'77188',NULL),(12,'RICARD','RICARD','R@RRR.ES','*49A5E44A73FE78E55F44540A0AF1A6A068F347F0','2001-01-01','2018-06-13 12:06:00',1,1,0,0,'7777',NULL),(15,'PEPE','CACA ','oscar@gamil.com','*49A5E44A73FE78E55F44540A0AF1A6A068F347F0','2018-05-17','2018-06-16 19:46:10',6,1,0,0,'63768',NULL),(16,'David','Gomez','rena693@hotmail.com','*49A5E44A73FE78E55F44540A0AF1A6A068F347F0','2018-02-09','2018-06-17 11:25:44',3,1,0,0,'63768',NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuariospartidos`
--

DROP TABLE IF EXISTS `usuariospartidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuariospartidos` (
  `idcreador` int(11) NOT NULL COMMENT 'Identificador de cada uno de los jugadores que tiene un partido. No tiene nada que ver con el creador del partido',
  `idpartidos` int(11) DEFAULT NULL,
  `jugador1` int(11) DEFAULT NULL,
  `jugador2` int(11) DEFAULT NULL,
  `jugador3` int(11) DEFAULT NULL,
  `jugador4` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcreador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuariospartidos`
--

LOCK TABLES `usuariospartidos` WRITE;
/*!40000 ALTER TABLE `usuariospartidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuariospartidos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-06-18 12:34:25
