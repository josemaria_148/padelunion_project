<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.sjava.padelunion.*" %>
<%@page import="java.util.*" %>

<%

           
    //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
    request.setCharacterEncoding("UTF-8");
  //verificamos si la petición procede de un POST
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            // hemos recibido un POST, reccpilamos el resto de datos...
                
                int id = Integer.parseInt(request.getParameter("id"));
                String nombre = request.getParameter("nombre");
                String apellidos = request.getParameter("apellidos");
                String email = request.getParameter("email");
                String fechanacimiento = request.getParameter("fechanacimiento");
                int nivel = Integer.parseInt(request.getParameter("nivel")); 
                int mano =  Integer.parseInt(request.getParameter("mano")); 
                int sexo = Integer.parseInt(request.getParameter("sexo"));       
                int administrador = Integer.parseInt(request.getParameter("administrador"));
                String telefono = request.getParameter("telefono");

                //creamos nuevo objeto usuario, que reemplazará al actual del mismo id
                Usuario u = new Usuario(id, nombre, apellidos, email, MiLib.stringToDate(fechanacimiento), nivel, mano,
         sexo,  administrador,  telefono);
                UsuarioController.guardar(u);
                //redirigimos navegador a la página list.jsp
                response.sendRedirect("/padelunion/index.jsp");
        }

%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
</head>
<body>


<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>
<%
   Usuario usu = UsuarioController.getId(loginId);
%>
<div class="container">
  

  <%-- <div class="row">
  <div class="col">
    <h1>Editar usuario</h1>
  </div>
  </div>


  <div class="row">
  <div class="col-md-8"> --%>


  <form class="form-horizontal" action="#" method="POST">
   <fieldset>
   <legend class="text-center headerUserEdit">Modifica tus datos personales</legend>
  
    <div class="form-row">

      <div class="form-group col-md-3"></div>
    
      <div class="form-group col-md-3">

          <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text"><i class="fas fa-user"></i></div>
            </div>
            <input  name="nombre"  type="text" class="form-control" id="nombreInput" value="<%= usu.getNombre() %>" placeholder="Nombre">
          </div>
      </div>

      <div class="form-group col-md-3">

          <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text"><i class="fas fa-user"></i></div>
            </div>
            <input  name="apellidos"  type="text" class="form-control" id="duracionInput" value="<%= usu.getApellidos() %>" placeholder="Apellidos">
          </div>
      </div>

      <div class="form-group col-md-3"></div>
      
    </div>

    <div class="form-row">

      <div class="form-group col-md-3"></div>      

      <div class="form-group col-md-3">
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text"><i class="fas fa-phone"></i></div>
          </div>
            <input  name="telefono"  type="text" class="form-control" id="telefonoInput" value="<%= usu.getTelefono() %>" placeholder="Teléfono">
        </div>
      </div>      

      <div class="form-group col-md-3">
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text">@</div>
          </div>
            <%-- <label for="emailInput">Email</label> --%>
            <input  name="email"  type="text" class="form-control" id="emailInput" value="<%= usu.getEmail() %>" placeholder="Email">
        </div>
      </div>

      <div class="form-group col-md-3"></div>      

    </div>

    <div class="form-row">

      <div class="form-group col-md-3"></div>

      <div class="form-group col-md-3">
       <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text"><i class="fas fa-calendar-alt"></i></div>
          </div>
          <input  name="fechanacimiento"  type="date" class="form-control" id="fechanacimientoInput" value="<%= usu.getFechaNacimiento() %>" >
        </div>
      </div>
      
      <div class="form-group col-md-3">      
        
          <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text"><i class="fas fa-female"></i><i class="fas fa-male"></i></div>
          </div>
          <select class="form-control" name="sexo" type="text" class="form-control" id="sexoInput" >  

          <% if(usu.getSexo()==1) {%>
            <option selected value="1">Femenino</option>
            <option value = "0">Masculino</option>
          <%} else {%>
            <option value="1">Femenino</option> 
            <option selected value="0">Masculino</option> 
            <%}%>   
          </select>
        </div>
       
      </div>

      <div class="form-group col-md-3"></div>

    </div>
    
    <div class="form-row">

      <div class="form-group col-md-3"></div>
    
      <div class="form-group col-md-3">
   
          <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text"><i class="far fa-hand-paper"></i></div>
          </div>
          <select class="form-control" name="mano" type="text" class="form-control" id="manoInput" >  

          <% if(usu.getMano()==1) {%>
            <option selected value="1">Zurdo</option>
            <option value = "0">Diestro</option>
          <%} else {%>
            <option value="1">Zurdo</option> 
            <option selected value="0">Diestro</option> 
            <%}%>   
          </select>
          </div>
      </div>
      
      <div class="form-group col-md-3">      
    
          <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text"><i class="fas fa-signal"></i></div>
          </div>
            <select class="form-control" name="nivel" type="text" class="form-control" id="nivelInput">  
            
            <option value="1">Iniciación</option>  
            <option value="2">Intermedio</option>
            <option value="3">Intermedio alto</option>
            <option value="4">Avanzado</option>
            <option value="5">Competición</option>
            <option value="6">Profesional</option>
            <option value="7">Profesional máximo</option>   
            
            </select>
          </div>
      </div>

      <div class="form-group col-md-3"></div>

    </div>


    
      <!-- guardamos id en campo oculto! -->
        <input type="hidden" name="id" value="<%= usu.getId() %>">
        <input type="hidden" name="administrador" value="<%= usu.getAdministrador() %>">

    <div class="form-row">
   
        <div class="col-md-12 text-center">
          <button type="submit" class="btn btn-primary">Guardar</button>      
        </div>
    
    </div>     

  </fieldset>
  </form>



</div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>    


</body>
</html>
