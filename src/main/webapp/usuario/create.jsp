<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>
<%@page import="java.util.*" %>

<%
    boolean datosOk;

    // Es posible que lleguemos aquí desde index principal, con una llamada GET
    // o bien como resultado del envío del formulario, con una llamada POST
    
    // si es un POST...
    if ("POST".equalsIgnoreCase(request.getMethod())) {
        // hemos recibido un POST, deberíamos tener datos del nuevo alumno
    
        //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
        request.setCharacterEncoding("UTF-8");

        String nombre = request.getParameter("nombre");
        String apellidos = request.getParameter("apellidos");
        String email = request.getParameter("email");
        String clave = request.getParameter("clave"); 
        Date fechanacimiento =  MiLib.stringToDate(request.getParameter("fechanacimiento"));
        int nivel = Integer.parseInt(request.getParameter("nivel")); 
        int mano =  Integer.parseInt(request.getParameter("mano")); 
        int sexo = Integer.parseInt(request.getParameter("sexo"));       
        int administrador = Integer.parseInt(request.getParameter("administrador"));
        String telefono = request.getParameter("telefono");
        
        // aquí verificaríamos que todo ok, y solo si todo ok hacemos guardar, 
        // por el momento lo damos por bueno...
        datosOk=true;
        if (datosOk){
            Usuario u = new Usuario( nombre, apellidos, email,  clave, fechanacimiento, nivel, mano,
         sexo,  administrador,  telefono);
            UsuarioController.guardar(u);
            // nos vamos a mostrar la lista, que ya contendrá el nuevo curso
            response.sendRedirect("/padelunion/usuario/list.jsp");
        }
    }


%>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Padel Union App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
</head>
<body>


<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="container">
<div class="row">
<div class="col">
<h1>Nuevo usuario</h1>
</div>
</div>


<div class="row">
<div class="col-md-8">

<form action="#" method="POST">
  <div class="form-group">
    <label for="nombreInput">Nombre</label>
    <input  name="nombre"  type="text" class="form-control" id="nombreInput" >
  </div>
    <div class="form-group">
    <label for="apellidosInput">Apellidos</label>
    <input  name="apellidos"  type="text" class="form-control" id="apellidosInput" >
  </div>

   <div class="form-group">
    <label for="emailInput">Email</label>
    <input  name="email"  type="email" class="form-control" id="emailInput">
  </div>

   </div>
      <div class="form-group">
    <label for="claveInput">Password</label>
    <input  name="clave"  type="password" class="form-control" id="claveInput" >
  </div>
  
 <div class="form-group">
    <label for="telefonoInput">Teléfono</label>
    <input  name="telefono"  type="text" class="form-control" id="telefonoInput">
  </div>

    <div class="form-group">
    <label for="manoInput">Mano dominante</label>
      <select  name="mano" type="text" class="form-control" id="manoInput" >  

      <option  value="0">Diestro</option>   
      <option value="1">Zurdo</option>  
      
      </select>
  </div>

  <div class="form-group">
    <label for="nivelInput">Nivel</label>
      <select  name="nivel" type="number" class="form-control" id="nivelInput" >  
      
      <option value="1">Iniciación</option>  
      <option value="2">Iniciación intermedio</option>
      <option value="3">Intermedio</option>
      <option value="4">Intermedio alto</option>
      <option value="5">Avanzado</option>
      <option value="6">Competición</option>
      <option value="7">Profesional</option>   
      
      </select>
  </div>


   <div class="form-group">
    <label for="sexoInput">Sexo</label>
    <select class="form-control" name="sexo" type="text" class="form-control" id="sexoInput" >  

      <option  value="0">Masculino</option>
    
      <option value="1">Femenino</option>  
      
    </select>
 
  </div>

      <div class="form-group">
    <label for="fechanacimientoInput">Fecha de nacimiento</label>
    <input  name="fechanacimiento"  type="date" class="form-control" id="fechanacimientoInput" >
  </div>
 </div>
  
    <input type="hidden" name="administrador" value="0">  
    <button type="submit" class="btn btn-primary">Guardar</button>
</form>


</div>
</div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>




</body>
</html>
