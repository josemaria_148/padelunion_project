<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="com.sjava.padelunion.*" %>
<%@page import="java.util.*" %>

<%

    Usuario u = null;
            
    //IMPORTANTISIMO!!! ANTES DE CUALQUIER GETPARAMETER!!!    
    request.setCharacterEncoding("UTF-8");

    String id = request.getParameter("id");
    if (id==null){
        //no recibimos id, debe ser un error... volvemos a index
        response.sendRedirect("/padelunion");
    }else{
        // recibimos id, puede ser que
        // 1) llegue por GET, desde list.jsp: mostraremos los datos para que puedan ser editados
        // 2) llegue por POST, junto con el resto de datos, para guardar los cambios

        //verificamos si la petición procede de un POST
        if ("POST".equalsIgnoreCase(request.getMethod())) {
            // hemos recibido un POST, reccpilamos el resto de datos...
                
                int id_numerico = Integer.parseInt(id);
                String nombre = request.getParameter("nombre");
                String apellidos = request.getParameter("apellidos");
                String email = request.getParameter("email");
                String fechanacimiento = request.getParameter("fechanacimiento");
                int nivel = Integer.parseInt(request.getParameter("nivel")); 
                int mano =  Integer.parseInt(request.getParameter("mano")); 
                int sexo = Integer.parseInt(request.getParameter("sexo"));       
                int administrador = Integer.parseInt(request.getParameter("administrador"));
                String telefono = request.getParameter("telefono");

                //creamos nuevo objeto usuario, que reemplazará al actual del mismo id
                u = new Usuario( id_numerico,  nombre, apellidos, email, MiLib.stringToDate(fechanacimiento), nivel, mano,
         sexo,  administrador,  telefono);
                UsuarioController.guardar(u);
                //redirigimos navegador a la página list.jsp
                response.sendRedirect("/padelunion/usuario/list.jsp");
        } else {
            // hemos recibido un GET, solo tenemos un id
            // pedimos los datos del alumno para mostrarlos en el formulario de edifión
            u = UsuarioController.getId(Integer.parseInt(id));
            if (u==null){
                //no recibimos id, debe ser un error... volvemos a index
             response.sendRedirect("/padelunion/index.jsp");
             }
        }
		
    }

%>

<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilos.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="/padelunion/css/estilosdavid.css">
</head>
<body>


<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="container">

  <div class="row">
  <div class="col">
    <h1>Editar usuario</h1>
  </div>
  </div>


  <div class="row">
  <div class="col-md-8">


  <form action="#" method="POST">
    <div class="form-group">
      <label for="nombreInput">Nombre</label>
      <input  name="nombre"  type="text" class="form-control" id="nombreInput" value="<%= u.getNombre() %>">
    </div>

    <div class="form-group">
      <label for="apellidosInput">Apellidos</label>
      <input  name="apellidos"  type="text" class="form-control" id="duracionInput" value="<%= u.getApellidos() %>">
    </div>

    <div class="form-group">
      <label for="emailInput">Email</label>
      <input  name="email"  type="text" class="form-control" id="emailInput" value="<%= u.getEmail() %>">
    </div>

    <div class="form-group">
      <label for="fechanacimientoInput">Fecha de nacimiento</label>
      <input  name="fechanacimiento"  type="date" class="form-control" id="fechanacimientoInput" value="<%= u.getFechaNacimiento() %>" >
    </div>

    <div class="form-group">
      <label for="telefonoInput">Telefono</label>
      <input  name="telefono"  type="text" class="form-control" id="telefonoInput" value="<%= u.getTelefono() %>">
    </div>

    <div class="form-group">
      <label for="manoInput">Mano Dominante</label>
      <select class="form-control" name="mano" type="text" class="form-control" id="manoInput" >  

      <% if(u.getMano()==0) {%>
        <option selected value="0">Zurdo</option>
        <option value = "1">Diestro</option>
      <%} else {%>
        <option value="0">Zurdo</option> 
        <option selected value="1">Diestro</option> 
        <%}%>   
      </select>
    </div>

    <div class="form-group">
      <label for="sexoInput">Sexo</label>
      <select class="form-control" name="sexo" type="text" class="form-control" id="sexoInput" >  

      <% if(u.getSexo()==1) {%>
        <option selected value="1">Femenino</option>
        <option value = "0">Masculino</option>
      <%} else {%>
        <option value="1">Femenino</option> 
        <option selected value="0">Masculino</option> 
        <%}%>   
      </select>
    </div>

    <div class="form-group">
      <label for="nivelInput">Nivel</label>
        <select class="form-control" name="nivel" type="text" class="form-control" id="nivelInput">  
        
        <option value="1">1</option>  
        <option value="2">2</option>
        <option value="3">3</option>
        <option value="4">4</option>
        <option value="5">5</option>
        <option value="6">6</option>
        <option value="7">7</option>   
        
        </select>
    </div>


  
    <!-- guardamos id en campo oculto! -->
      <input type="hidden" name="id" value="<%= u.getId() %>">
      <input type="hidden" name="administrador" value="<%= u.getAdministrador() %>">

      <button type="submit" class="btn btn-primary">Guardar</button>
  </form>



</div>
</div>

</div>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>    


</body>
</html>
