<%@page contentType="text/html;charset=UTF-8" %>
<%@page import="com.sjava.padelunion.*" %>


<!DOCTYPE html>
<html lang="es-ES">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>PadelUnion App</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="../css/estilos.css">
    <link rel="stylesheet" type="text/css" href="../css/estilospepe.css">
    <link rel="stylesheet" type="text/css" href="../css/estilosdavid.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
</head>
<body>


<%@include file="/parts/login.jsp"%>
<%@include file="/parts/menu.jsp"%>

<div class="container">

<div class="row">
<div class="col">
<h1>Listado Usuarios</h1>
</div>
</div>


<div class="row">
<div class="col">


<table class="table" id="tabla_listado">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellidos</th>
      <th scope="col">Email</th>
     <th scope="col">Fecha de nacimiento</th>
     <th scope="col">Nivel</th>
     <th scope="col">Mano</th>
     <th scope="col">Sexo</th>
     <th scope="col">Administrador</th>
     <th scope="col">Teléfono</th>
     
    </tr>
  </thead>
  <tbody>
   <% for (Usuario u : UsuarioController.getAll()) { %>
        <tr>
        <th scope="row"><%= u.getId() %></th>
        <td><%= u.getNombre() %></td>
        <td><%= u.getApellidos() %></td>
        <td><%= u.getEmail() %></td>
        <td><%= u.getFechaNacimiento() %></td>
        <td><%= CalculaNivel.nivel(u.getNivel()) %></td>
        <td><% if(u.getMano()==0){%>Zurdo<%} else {%>Diestro<%}%></td>
        <td><% if(u.getSexo()==0){%>Masculino<%} else {%>Femenino<%} %></td>
        <td><%= u.getAdministrador() %></td>
        <td><%= u.getTelefono() %></td>
        <% if (loginId>0) { %>
        <td><a href="/padelunion/usuario/edit.jsp?id=<%= u.getId() %>"><i class="fas fa-pencil-alt"></i>
        </a></td>
                <td><a href="/padelunion/usuario/remove.jsp?id=<%= u.getId() %>"><i class="fas fa-trash-alt"></i>
        </a></td>
        <% } %>
        </tr>
    <% } %>
  </tbody>
</table>

<a href="/padelunion/usuario/create.jsp" class="btn btn-sm btn-primary">Nuevo usuario</a>
<%-- <div>Hay  <% out.print(UsuarioController.size()); %></div> --%>
</div>
</div>


</div>


    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
    <script src="/padelunion/js/scripts.js"></script>


</body>
</html>
